const mix = require('laravel-mix');
const config = require('./webpack.config');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');

mix.copyDirectory('resources/media', 'public/media');

// mix.styles([
//     'public/vendor/core/stimulsoft/css/stimulsoft.viewer.office2013.whiteteal.css',
//     'public/vendor/core/stimulsoft/css/stimulsoft.designer.office2013.lightgrayteal.css'
// ], 'public/css/print.css');

mix.scripts([
    'public/vendor/core/stimulsoft/scripts/stimulsoft.reports.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.reports.maps.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.dashboards.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.viewer.js',
    'public/vendor/core/stimulsoft/scripts/stimulsoft.designer.js'
], 'public/js/print.js');

mix.sourceMaps();
mix.webpackConfig(config);

if (mix.inProduction()) {
    mix.version();
}

