<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'Auth\AuthController@login')->name('login');
Route::get('verify', 'Auth\AuthController@verify');

Route::group(['prefix' => 'olap'], function () {
    Route::group(['prefix' => 'print'], function () {
        Route::get('/', 'OLAP\OPrintController@index')->name('olap.print');
        Route::post('/', 'OLAP\OPrintController@index');
    });
    Route::group(['prefix' => 'report'], function () {
        Route::group(['prefix' => 'detail'], function () {
            Route::get('/', 'OLAP\OReportController@indexDetail')->name('olap.report.detail');
            Route::post('/', 'OLAP\OReportController@postDetail');
        });
        Route::group(['prefix' => 'list'], function () {
            Route::get('/', 'OLAP\OReportController@indexList')->name('olap.report.list');
        });
    });
});

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get(env('MIX_API_VERIFY'), 'Auth\AuthController@verify');

    Route::get('/', 'Admin\Dashboard\ADashboardController@index')->name('home');
    Route::group(['prefix' => 'tools'], function () {
        Route::get('barang_kategori', 'Admin\Tools\Select2@barangKategori')->name('tools.s2.barang_kategori');
        Route::get('mekanik', 'Admin\Tools\Select2@mekanik')->name('tools.s2.mekanik');
        Route::get('pelanggan', 'Admin\Tools\Select2@pelanggan')->name('tools.s2.pelanggan');
        Route::get('supplier', 'Admin\Tools\Select2@supplier')->name('tools.s2.supplier');
        Route::get('barang', 'Admin\Tools\Select2@barang')->name('tools.s2.barang');
        Route::get('satuan', 'Admin\Tools\Select2@satuanItem')->name('tools.s2.satuan');
        Route::get('unit', 'Admin\Tools\Select2@unit')->name('tools.s2.unit');

        Route::get('user_cabang', 'Admin\Tools\Popup@getUserCabang')->name('tools.user_cabang');
        Route::post('user_cabang', 'Admin\Tools\Popup@postUserCabang')->name('post.tools.user_cabang');
    });
    Route::group(['prefix' => 'sistem'], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::get('del', 'Admin\Sistem\ASistemUserController@deleteData')->name('user.delete');
            Route::get('ganti-password', 'Admin\Sistem\ASistemUserController@ganti_password')->name('password.reset');
            Route::post('update', 'Admin\Sistem\ASistemUserController@updatePassword')->name('password.update');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Sistem\ASistemUserController@indexDetail')->name('user.detail');
                Route::post('/', 'Admin\Sistem\ASistemUserController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Sistem\ASistemUserController@indexList')->name('user.list');
            });
        });
        Route::group(['prefix' => 'role_user'], function () {
            Route::get('s2', 'Admin\Sistem\ASistemRoleUserController@searchData')->name('s2.role_user');
            Route::get('del', 'Admin\Sistem\ASistemRoleUserController@deleteData')->name('role_user.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexDetail')->name('role_user.detail');
                Route::post('/', 'Admin\Sistem\ASistemRoleUserController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Sistem\ASistemRoleUserController@indexList')->name('role_user.list');
            });
        });
    });
    Route::group(['prefix' => 'procurement'], function () {
        Route::group(['prefix' => 'pembelian'], function () {
            Route::get('del', 'Admin\Procurement\APembelianController@deleteData')->name('pembelian.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Procurement\APembelianController@indexDetail')->name('pembelian.detail');
                Route::post('/', 'Admin\Procurement\APembelianController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Procurement\APembelianController@indexList')->name('pembelian.list');
            });
        });
    });
    Route::group(['prefix' => 'sales'], function () {
        Route::group(['prefix' => 'penjualan'], function () {
            Route::get('del', 'Admin\Sales\APenjualanController@deleteData')->name('penjualan.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Sales\APenjualanController@indexDetail')->name('penjualan.detail');
                Route::post('/', 'Admin\Sales\APenjualanController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Sales\APenjualanController@indexList')->name('penjualan.list');
            });
        });
    });
    Route::group(['prefix' => 'project'], function () {
        Route::group(['prefix' => 'work_order'], function () {
            Route::get('del', 'Admin\Project\AWorkOrderController@deleteData')->name('work_order.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Project\AWorkOrderController@indexDetail')->name('work_order.detail');
                Route::post('/', 'Admin\Project\AWorkOrderController@postDetail');
            });
            Route::group(['prefix' => 'barang'], function () {
                Route::get('/', 'Admin\Project\AWorkOrderBarangController@indexDetail')->name('work_order.barang.detail');
                Route::post('/', 'Admin\Project\AWorkOrderBarangController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Project\AWorkOrderController@indexList')->name('work_order.list');
                Route::get('status', 'Admin\Project\AWorkOrderController@setStatus')->name('work_order.status');
            });
            Route::group(['prefix' => 'upload'], function () {
                Route::get('/', 'Admin\Project\AWorkOrderUploadController@indexDetail')->name('work_order.upload.detail');
                Route::post('/', 'Admin\Project\AWorkOrderUploadController@postDetail');
            });
        });
    });
    Route::group(['prefix' => 'master'], function () {
        Route::group(['prefix' => 'kelas'], function () {
            Route::get('del', 'Admin\Master\AKelasController@deleteData')->name('kelas.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AKelasController@indexDetail')->name('kelas.detail');
                Route::post('/', 'Admin\Master\AKelasController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AKelasController@indexList')->name('kelas.list');
            });
        });
        Route::group(['prefix' => 'kategori'], function () {
            Route::get('del', 'Admin\Master\AKategoriController@deleteData')->name('kategori.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AKategoriController@indexDetail')->name('kategori.detail');
                Route::post('/', 'Admin\Master\AKategoriController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AKategoriController@indexList')->name('kategori.list');
            });
        });
        Route::group(['prefix' => 'sabuk'], function () {
            Route::get('del', 'Admin\Master\ASabukController@deleteData')->name('sabuk.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ASabukController@indexDetail')->name('sabuk.detail');
                Route::post('/', 'Admin\Master\ASabukController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ASabukController@indexList')->name('sabuk.list');
            });
        });
        Route::group(['prefix' => 'pengkot'], function () {
            Route::get('del', 'Admin\Master\ApengkotController@deleteData')->name('pengkot.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ApengkotController@indexDetail')->name('pengkot.detail');
                Route::post('/', 'Admin\Master\ApengkotController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ApengkotController@indexList')->name('pengkot.list');
            });
        });
        Route::group(['prefix' => 'klub'], function () {
            Route::get('del', 'Admin\Master\AKlubController@deleteData')->name('klub.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AKlubController@indexDetail')->name('klub.detail');
                Route::post('/', 'Admin\Master\AKlubController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AKlubController@indexList')->name('klub.list');
            });
        });
        Route::group(['prefix' => 'barang'], function () {
            Route::get('del', 'Admin\Master\ABarangController@deleteData')->name('barang.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ABarangController@indexDetail')->name('barang.detail');
                Route::post('/', 'Admin\Master\ABarangController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ABarangController@indexList')->name('barang.list');
            });
            Route::group(['prefix' => 'price'], function () {
                Route::group(['prefix' => 'penjualan'], function () {
                    Route::group(['prefix' => 'detail'], function () {
                        Route::get('/', 'Admin\Master\ABarangPriceJualController@indexDetail')->name('barang_price_penjualan.detail');
                        Route::post('/', 'Admin\Master\ABarangPriceJualController@postDetail');
                    });
                    Route::group(['prefix' => 'list'], function () {
                        Route::get('/', 'Admin\Master\ABarangPriceJualController@indexList')->name('barang_price_penjualan.list');
                    });
                });
                Route::group(['prefix' => 'pembelian'], function () {
                    Route::group(['prefix' => 'detail'], function () {
                        Route::get('/', 'Admin\Master\ABarangPriceBeliController@indexDetail')->name('barang_price_pembelian.detail');
                        Route::post('/', 'Admin\Master\ABarangPriceBeliController@postDetail');
                    });
                    Route::group(['prefix' => 'list'], function () {
                        Route::get('/', 'Admin\Master\ABarangPriceBeliController@indexList')->name('barang_price_pembelian.list');
                    });
                });
            });
        });
        Route::group(['prefix' => 'unit'], function () {
            Route::get('del', 'Admin\Master\AUnitController@deleteData')->name('unit.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AUnitController@indexDetail')->name('unit.detail');
                Route::post('/', 'Admin\Master\AUnitController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AUnitController@indexList')->name('unit.list');
            });
        });
        Route::group(['prefix' => 'pelanggan'], function () {
            Route::get('del', 'Admin\Master\APelangganController@deleteData')->name('pelanggan.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\APelangganController@indexDetail')->name('pelanggan.detail');
                Route::post('/', 'Admin\Master\APelangganController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\APelangganController@indexList')->name('pelanggan.list');
            });
        });
        Route::group(['prefix' => 'mekanik'], function () {
            Route::get('del', 'Admin\Master\AMekanikController@deleteData')->name('mekanik.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AMekanikController@indexDetail')->name('mekanik.detail');
                Route::post('/', 'Admin\Master\AMekanikController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AMekanikController@indexList')->name('mekanik.list');
            });
        });
        Route::group(['prefix' => 'barang_kategori'], function () {
            Route::get('del', 'Admin\Master\ABarangKategoriController@deleteData')->name('barang_kategori.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ABarangKategoriController@indexDetail')->name('barang_kategori.detail');
                Route::post('/', 'Admin\Master\ABarangKategoriController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ABarangKategoriController@indexList')->name('barang_kategori.list');
            });
        });
        Route::group(['prefix' => 'supplier'], function () {
            Route::get('del', 'Admin\Master\ASupplierController@deleteData')->name('supplier.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ASupplierController@indexDetail')->name('supplier.detail');
                Route::post('/', 'Admin\Master\ASupplierController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ASupplierController@indexList')->name('supplier.list');
            });
        });
        Route::group(['prefix' => 'uang_kerja'], function () {
            Route::get('del', 'Admin\Master\AUangKerjaController@deleteData')->name('uang_kerja.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AUangKerjaController@indexDetail')->name('uang_kerja.detail');
                Route::post('/', 'Admin\Master\AUangKerjaController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AUangKerjaController@indexList')->name('uang_kerja.list');
            });
        });
        Route::group(['prefix' => 'muatan_luar'], function () {
            Route::get('del', 'Admin\Master\AMuatanLuarController@deleteData')->name('muatan_luar.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AMuatanLuarController@indexDetail')->name('muatan_luar.detail');
                Route::post('/', 'Admin\Master\AMuatanLuarController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AMuatanLuarController@indexList')->name('muatan_luar.list');
            });
        });
        Route::group(['prefix' => 'muatan_dalam'], function () {
            Route::get('del', 'Admin\Master\AMuatanDalamController@deleteData')->name('muatan_dalam.delete');
            Route::get('app', 'Admin\Master\AMuatanDalamController@approve');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AMuatanDalamController@indexDetail')->name('muatan_dalam.detail');
                Route::post('/', 'Admin\Master\AMuatanDalamController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AMuatanDalamController@indexList')->name('muatan_dalam.list');
            });
        });
        Route::group(['prefix' => 'biaya_umum'], function () {
            Route::get('del', 'Admin\Master\ABiayaUmumController@deleteData')->name('biaya_umum.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ABiayaUmumController@indexDetail')->name('biaya_umum.detail');
                Route::post('/', 'Admin\Master\ABiayaUmumController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ABiayaUmumController@indexList')->name('biaya_umum.list');
            });
        });
        Route::group(['prefix' => 'biaya_kir'], function () {
            Route::get('del', 'Admin\Master\ABiayaKirController@deleteData')->name('biaya_kir.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ABiayaKirController@indexDetail')->name('biaya_kir.detail');
                Route::post('/', 'Admin\Master\ABiayaKirController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ABiayaKirController@indexList')->name('biaya_kir.list');
            });
        });
        Route::group(['prefix' => 'truck'], function () {
            Route::get('del', 'Admin\Master\ATruckController@deleteData')->name('truck.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ATruckController@indexDetail')->name('truck.detail');
                Route::post('/', 'Admin\Master\ATruckController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ATruckController@indexList')->name('truck.list');
            });
        });
        Route::group(['prefix' => 'equipment_expenses'], function () {
            Route::get('status', 'Admin\Master\AEquipmentController@setStatus')->name('equipment_expenses.status');
            Route::get('del', 'Admin\Master\AEquipmentController@deleteData')->name('equipment_expenses.delete');
            Route::get('hapus', 'Admin\Master\AEquipmentController@deleteDataDetail')->name('equipment_expenses.delete2');
            Route::get('detail', 'Admin\Master\AEquipmentController@indexDetail')->name('equipment_expenses.detail');
            Route::post('detail_log', 'Admin\Master\AEquipmentController@postDetail')->name('equipment_expenses.log');
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AEquipmentController@indexList')->name('equipment_expenses.list');
                Route::post('/', 'Admin\Master\AEquipmentController@postModel');
            });
        });
        Route::group(['prefix' => 'uang_kerja'], function () {
            Route::get('del', 'Admin\Master\AUangKerjaController@deleteData')->name('uang_kerja.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\AUangKerjaController@indexDetail')->name('uang_kerja.detail');
                Route::post('/', 'Admin\Master\AUangKerjaController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\AUangKerjaController@indexList')->name('uang_kerja.list');
            });
        });
        Route::group(['prefix' => 'biaya_staff'], function () {
            Route::get('del', 'Admin\Master\ABiayaStaffController@deleteData')->name('biaya_staff.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Master\ABiayaStaffController@indexDetail')->name('biaya_staff.detail');
                Route::post('/', 'Admin\Master\ABiayaStaffController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Master\ABiayaStaffController@indexList')->name('biaya_staff.list');
            });
        });
    });
    Route::group(['prefix' => 'event'], function () {
        Route::group(['prefix' => 'acara'], function () {
            Route::get('del', 'Admin\Event\AAcaraController@deleteData')->name('acara.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Event\AAcaraController@indexDetail')->name('acara.detail');
                Route::post('/', 'Admin\Event\AAcaraController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Event\AAcaraController@indexList')->name('acara.list');
                Route::group(['prefix' => 'guest'], function () {
                    Route::get('mail', 'Admin\Event\AAcaraGuestController@sendEmail')->name('acara_guest.email');
                    Route::group(['prefix' => 'detail'], function () {
                        Route::get('/', 'Admin\Event\AAcaraGuestController@indexDetail')->name('acara_guest.detail');
                        Route::post('/', 'Admin\Event\AAcaraGuestController@postDetail');
                    });
                    Route::group(['prefix' => 'list'], function () {
                        Route::get('del', 'Admin\Event\AAcaraGuestController@deleteData')->name('acara_guest.delete');
                        Route::get('/', 'Admin\Event\AAcaraGuestController@indexList')->name('acara_guest.list');
                        Route::post('/', 'Admin\Event\AAcaraGuestController@import')->name('acara_guest.import');
                    });
                });
                Route::group(['prefix' => 'foto'], function () {
                    Route::get('/', 'Admin\Event\AAcaraFotoController@indexDetail')->name('acara_foto.upload.detail');
                    Route::post('/', 'Admin\Event\AAcaraFotoController@postDetail')->name('acara_foto.upload');
                });
            });
        });
        Route::group(['prefix' => 'booking'], function () {
            Route::get('del', 'Admin\Event\ABookingController@deleteData')->name('booking.delete');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Event\ABookingController@indexDetail')->name('booking.detail');
                Route::post('/', 'Admin\Event\ABookingController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Event\ABookingController@indexList')->name('booking.list');
            });
        });
    });
    Route::group(['prefix' => 'invoice'], function () {
        Route::group(['prefix' => 'data_uang_kerja'], function () {
            Route::get('del', 'Admin\Invoice\ADataUangKerjaController@deleteData')->name('data_uang_kerja.delete');
//                Route::get('/', 'Admin\Invoice\ADataUangKerjaController@indexDetail')->name('data_uang_kerja.detail');
            Route::post('/', 'Admin\Invoice\ADataUangKerjaController@postDetail');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Invoice\ADataUangKerjaController@indexDetail')->name('data_uang_kerja.detail');
                Route::post('/', 'Admin\Invoice\ADataUangKerjaController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Invoice\ADataUangKerjaController@indexList')->name('data_uang_kerja.list');
            });
        });
        Route::group(['prefix' => 'data_trip'], function () {
            Route::get('del', 'Admin\Invoice\ADataTripController@deleteData')->name('data_trip.delete');
//                Route::get('/', 'Admin\Invoice\ADataTripController@indexDetail')->name('data_trip.detail');
            Route::post('/', 'Admin\Invoice\ADataTripController@postDetail');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Invoice\ADataTripController@indexDetail')->name('data_trip.detail');
                Route::post('/', 'Admin\Invoice\ADataTripController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Invoice\ADataTripController@indexList')->name('data_trip.list');
            });
        });
        Route::group(['prefix' => 'kwitansi'], function () {
            Route::get('del', 'Admin\Invoice\ADataController@deleteData')->name('kwitansi.delete');
            Route::get('add', 'Admin\Invoice\ADataController@postInvoice')->name('kwitansi.add');
            Route::get('audit', 'Admin\Invoice\ADataController@audit')->name('kwitansi.audit');
            Route::get('hapus', 'Admin\Invoice\ADataController@deleteDataDetail')->name('kwitansi.delete_detail');
            Route::get('pembayaran', 'Admin\Invoice\ADataController@pembayaran')->name('kwitansi.pembayaran');
            Route::post('pelunasan', 'Admin\Invoice\ADataController@postPembayaran')->name('kwitansi.pelunasan');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Invoice\ADataController@indexDetail')->name('kwitansi.detail');
                Route::post('/', 'Admin\Invoice\ADataController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Invoice\ADataController@indexList')->name('kwitansi.list');
            });
        });
        Route::group(['prefix' => 'kwitansi_uang_kerja'], function () {
            Route::get('del', 'Admin\Invoice\ADataController@deleteData')->name('kwitansi_uang_kerja.delete');
            Route::get('add', 'Admin\Invoice\ADataController@postInvoice')->name('kwitansi_uang_kerja.add');
            Route::get('audit', 'Admin\Invoice\ADataController@audit')->name('kwitansi_uang_kerja.audit');
            Route::get('pembayaran', 'Admin\Invoice\ADataController@pembayaran')->name('kwitansi_uang_kerja.pembayaran');
            Route::post('pelunasan', 'Admin\Invoice\ADataController@postPembayaran')->name('kwitansi_uang_kerja.pelunasan');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Invoice\ADataController@indexDetailKwitansi')->name('kwitansi_uang_kerja.detail');
                Route::post('/', 'Admin\Invoice\ADataController@postDetailKwitansi');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Invoice\ADataController@indexListKwitansi')->name('kwitansi_uang_kerja.list');
            });
        });
        Route::group(['prefix' => 'approve'], function () {
            Route::get('del', 'Admin\Invoice\AApproveController@deleteData')->name('approve.delete');
            Route::get('app', 'Admin\Invoice\AApproveController@approve')->name('approve.app');
            Route::get('deskripsi', 'Admin\Invoice\AApproveController@deskripsi')->name('approve.deskripsi');
            Route::post('post', 'Admin\Invoice\AApproveController@post_deskripsi')->name('deskripsi.post');
            Route::get('print', 'Admin\Invoice\AApproveController@print')->name('approve.print');
            Route::group(['prefix' => 'detail'], function () {
                Route::get('/', 'Admin\Invoice\AApproveController@indexDetail')->name('approve.detail');
                Route::post('/', 'Admin\Invoice\AApproveController@postDetail');
            });
            Route::group(['prefix' => 'list'], function () {
                Route::get('/', 'Admin\Invoice\AApproveController@indexList')->name('approve.list');
            });
        });
    });
    Route::group(['prefix' => 'report'], function () {
        Route::get('/', 'Admin\OLAP\AReportController@generateReport')->name('report');
        Route::group(['prefix' => 'today-all'], function () {
            Route::get('/', 'Admin\OLAP\AReportController@indexTodayAll')->name('report.today-all');
        });
        Route::group(['prefix' => 'all'], function () {
            Route::get('/', 'Admin\OLAP\AReportController@indexAll')->name('report.all');
        });
    });
});
