<?php

namespace App\Constants;

class Constant
{
    const AlertWarning = 'alert-warning';
    const AlertDanger = 'alert-danger';
    const AlertSuccess = 'alert-success';
    const AlertPrimary = 'alert-primary';

    const TitleWarning = 'Warning! ';
    const TitleError = 'Error! ';
    const TitleSuccess = 'Success! ';

    const DefConnection = 'mysql';

    const ParamSegment = "~"; // ~NAME:STRING~
    const ParamSeparator = ":"; // ~NAME:TIPE~ *TIPE DI REPLACE -> (CHOICE *COMBOBOX, STRING *INPUTTEXT, DATETIME *INPUTDATE)
    const ParamChoiceSegment = '^'; // ~NAME:CHOICE^select 1 as id, 2 as label^~ wajib ada id & label

    const PrintInvoice = 'Invoice';
    const PrintOrderKerja = 'OrderKerja';
    const PrintPembelian = 'Pembelian';
    const PrintOrderBarang = 'OrderBarang';
    const PrintAcara = 'Acara';

    const ReportGeneral = 'GENERAL';
    const ReportLine = 'LINE';
    const ReportBar = 'BAR';
    const ReportPie = 'PIE';

    const ResponseSuccess = 'SUCCESS';
    const ResponseError = 'ERROR';
}
