<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PembelianDetail
 *
 * @property int $id
 * @property int $pembelian_id
 * @property int $barang_id
 * @property int $qty
 * @property float $harga_satuan
 * @property float $subtotal
 *
 * @property Barang $barang
 * @property Pembelian $pembelian
 *
 * @package App\Models
 */
class PembelianDetail extends Model
{
	protected $table = 'pembelian_detail';
	public $timestamps = false;

	protected $casts = [
		'pembelian_id' => 'int',
		'barang_id' => 'int',
		'qty' => 'int',
		'harga_satuan' => 'float',
		'subtotal' => 'float'
	];

	protected $fillable = [
		'pembelian_id',
		'barang_id',
		'qty',
		'harga_satuan',
		'subtotal'
	];

	public function barang()
	{
		return $this->belongsTo(Barang::class,'barang_id');
	}

	public function pembelian()
	{
		return $this->belongsTo(Pembelian::class,'pembelian_id');
	}
}
