<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Controllers\Admin\Tools\Select2;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Mekanik
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|OrderKerja[] $order_kerjas
 *
 * @package App\Models
 */
class Mekanik extends Model
{
    use SoftDeletes;

    protected $table = 'mekanik';

    protected $fillable = [
        'kode',
        'nama'
    ];

    public function order_kerjas()
    {
        return $this->hasMany(OrderKerja::class);
    }

    public $appends = ['s2_value', 's2_text'];

    function getS2ValueAttribute()
    {
        return Select2::genS2Value($this->id, $this->s2_text);
    }

    function getS2TextAttribute()
    {
        return '(' . $this->kode . ') ' . $this->nama;
    }
}
