<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DataInvoicePrint
 *
 * @property int $id
 * @property int $data_invoice_header_id
 * @property int $approve
 * @property string $deskripsi
 * @property int $users_id
 * @property int $company_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property DataInvoiceHeader $data_invoice_header
 * @property User $user
 *
 * @package App\Models
 */
class DataInvoicePrint extends Model
{
	use SoftDeletes;
	protected $table = 'data_invoice_print';

	protected $casts = [
		'data_invoice_header_id' => 'int',
		'approve' => 'int',
		'users_id' => 'int',
        'company_id' => 'int'
	];

	protected $fillable = [
		'data_invoice_header_id',
		'approve',
		'deskripsi',
		'users_id',
        'company_id'
	];

	public function data_invoice_header()
	{
		return $this->belongsTo(DataInvoiceHeader::class);
	}

    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
