<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderKerjaFoto
 *
 * @property int $id
 * @property int $order_kerja_id
 * @property string $foto_url
 * @property Carbon $created_by
 * @property Carbon $updated_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property OrderKerja $order_kerja
 *
 * @package App\Models
 */
class OrderKerjaFoto extends Model
{
    protected $table = 'order_kerja_foto';

    protected $casts = [
        'order_kerja_id' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int'
    ];

    protected $fillable = [
        'order_kerja_id',
        'foto_url',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    ];

    public function order_kerja()
    {
        return $this->belongsTo(OrderKerja::class);
    }
}
