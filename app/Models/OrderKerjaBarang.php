<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderKerjaBarang
 *
 * @property int $id
 * @property int $order_kerja_id
 * @property Carbon $tgl
 * @property int $barang_id
 * @property int $qty
 * @property int $created_by
 *
 * @property Barang $barang
 * @property OrderKerja $order_kerja
 * @property User $user
 *
 * @package App\Models
 */
class OrderKerjaBarang extends Model
{
	protected $table = 'order_kerja_barang';
	public $timestamps = false;

	protected $casts = [
		'order_kerja_id' => 'int',
		'barang_id' => 'int',
		'qty' => 'int',
		'created_by' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'order_kerja_id',
		'tgl',
		'barang_id',
		'qty',
		'created_by'
	];

	public function barang()
	{
		return $this->belongsTo(Barang::class);
	}

	public function order_kerja()
	{
		return $this->belongsTo(OrderKerja::class);
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'order_kerja_id');
	}
}
