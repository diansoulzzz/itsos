<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Satuan
 *
 * @property int $id
 * @property int $barang_id
 * @property string $kode
 * @property float $rasio
 * @property string $keterangan
 *
 * @property Barang $barang
 * @property Collection|PenjualanDetail[] $penjualan_details
 *
 * @package App\Models
 */
class Satuan extends Model
{
	protected $table = 'satuan';
	public $timestamps = false;

	protected $casts = [
		'barang_id' => 'int',
		'rasio' => 'float'
	];

	protected $fillable = [
		'barang_id',
		'kode',
		'rasio',
		'keterangan'
	];

	public function barang()
	{
		return $this->belongsTo(Barang::class);
	}

	public function penjualan_details()
	{
		return $this->hasMany(PenjualanDetail::class);
	}
    function getIdAttribute()
    {
        $ar = [
            'id' => $this->id,
            'text' => $this->rasio . " (" . $this->kode . ")",
        ];
        return json_encode($ar);
    }

    function getTextAttribute()
    {
        return $this->rasio . " (" . $this->kode . ")";
    }
}
