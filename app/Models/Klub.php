<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Klub
 * 
 * @property int $id
 * @property int $pengkot_id
 * @property string $nama_klub
 * @property string $alamat
 * @property string $telpon
 * @property int $aktif
 * @property int $banned
 * @property string $keterangan
 * @property int $users_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property Pengkot $pengkot
 * @property User $user
 * @property Collection|Atlet[] $atlets
 *
 * @package App\Models
 */
class Klub extends Model
{
	use SoftDeletes;
	protected $table = 'klub';

	protected $casts = [
		'pengkot_id' => 'int',
		'aktif' => 'int',
		'banned' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'pengkot_id',
		'nama_klub',
		'alamat',
		'telpon',
		'aktif',
		'banned',
		'keterangan',
		'users_id'
	];

	public function pengkot()
	{
		return $this->belongsTo(Pengkot::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'users_id');
	}

	public function atlets()
	{
		return $this->hasMany(Atlet::class);
	}
}
