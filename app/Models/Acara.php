<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Acara
 *
 * @property int $id
 * @property Carbon $tgl
 * @property string $kode_nota
 * @property int $pelanggan_id
 * @property Carbon $waktu
 * @property string $lokasi
 * @property string $design
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Pelanggan $pelanggan
 * @property User $user
 * @property Collection|AcaraDetailFoto[] $acara_detail_fotos
 * @property Collection|AcaraDetailGuest[] $acara_detail_guests
 *
 * @package App\Models
 */
class Acara extends Model
{
    use SoftDeletes;

    protected $table = 'acara';
    protected $hidden = ['design'];
    protected $casts = [
        'pelanggan_id' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int'
    ];

    protected $dates = [
        'tgl',
        'waktu'
    ];

    protected $fillable = [
        'tgl',
        'kode_nota',
        'pelanggan_id',
        'waktu',
        'lokasi',
        'design',
        'created_by',
        'updated_by'
    ];

    public function pelanggan()
    {
        return $this->belongsTo(Pelanggan::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function acara_detail_fotos()
    {
        return $this->hasMany(AcaraDetailFoto::class);
    }

    public function acara_detail_guests()
    {
        return $this->hasMany(AcaraDetailGuest::class);
    }

    public function acara_detail_guests_email()
    {
        return $this->hasMany(AcaraDetailGuest::class)->whereNotNull('email');
    }
}
