<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AcaraDetailFoto
 * 
 * @property int $id
 * @property int $acara_id
 * @property string $foto_url
 * @property int $created_by
 * @property Carbon $created_at
 * 
 * @property Acara $acara
 * @property User $user
 *
 * @package App\Models
 */
class AcaraDetailFoto extends Model
{
	protected $table = 'acara_detail_foto';
	public $timestamps = false;

	protected $casts = [
		'acara_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'acara_id',
		'foto_url',
		'created_by'
	];

	public function acara()
	{
		return $this->belongsTo(Acara::class);
	}

	public function user()
	{
		return $this->belongsTo(User::class, 'created_by');
	}
}
