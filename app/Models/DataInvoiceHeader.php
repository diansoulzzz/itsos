<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DataInvoiceHeader
 *
 * @property int $id
 * @property string $no_invoice
 * @property string $no_trip
 * @property string $no_pol
 * @property int $nilai_awal_total
 * @property int $total_tagihan
 * @property int $ppn
 * @property string $kepada
 * @property string $penyewa
 * @property string $keterangan
 * @property string $alamat
 * @property Carbon $tanggal
 * @property string $status
 * @property string $checker
 * @property int $flag
 * @property int $users_id
 * @property int $company_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property User $user
 * @property Collection|DataInvoiceDetail[] $data_invoice_details
 * @property Collection|DataInvoicePrint[] $data_invoice_prints
 * @property Collection|Pelunasan[] $pelunasans
 *
 * @package App\Models
 */
class DataInvoiceHeader extends Model
{
	use SoftDeletes;
	protected $table = 'data_invoice_header';

	protected $casts = [
		'nilai_awal_total' => 'int',
		'ppn' => 'int',
		'flag' => 'int',
		'users_id' => 'int',
        'company_id' => 'int'
	];

	protected $dates = [
		'tanggal'
	];

	protected $fillable = [
		'no_invoice',
		'no_trip',
		'no_pol',
		'nilai_awal_total',
        'total_tagihan',
		'ppn',
		'kepada',
		'penyewa',
		'keterangan',
		'alamat',
		'tanggal',
		'status',
		'checker',
		'flag',
		'users_id',
        'company_id'
	];

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id');
    }

	public function data_invoice_details()
	{
		return $this->hasMany(DataInvoiceDetail::class);
	}

	public function data_invoice_prints()
	{
		return $this->hasMany(DataInvoicePrint::class);
	}

	public function pelunasans()
	{
		return $this->hasMany(Pelunasan::class);
	}
}
