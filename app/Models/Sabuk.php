<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Sabuk
 * 
 * @property int $id
 * @property string $geup
 * @property string $keterangan
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Sabuk extends Model
{
	use SoftDeletes;
	protected $table = 'sabuk';

	protected $fillable = [
		'geup',
		'keterangan'
	];
}
