<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Dashboard
 * 
 * @property int $id
 * @property int $roles_id
 * @property int $report_id
 * 
 * @property Report $report
 * @property Role $role
 *
 * @package App\Models
 */
class Dashboard extends Model
{
	protected $table = 'dashboard';
	public $timestamps = false;

	protected $casts = [
		'roles_id' => 'int',
		'report_id' => 'int'
	];

	protected $fillable = [
		'roles_id',
		'report_id'
	];

	public function report()
	{
		return $this->belongsTo(Report::class);
	}

	public function role()
	{
		return $this->belongsTo(Role::class, 'roles_id');
	}
}
