<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Kategori
 * 
 * @property int $id
 * @property string $kategori
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Kategori extends Model
{
	use SoftDeletes;
	protected $table = 'kategori';

	protected $fillable = [
		'kategori'
	];
}
