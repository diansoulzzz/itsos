<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class DataMuatan
 *
 * @property int $id
 * @property string $kode_data
 * @property string $no_trip
 * @property string $penerima
 * @property int $jumlah
 * @property string $jenis_biaya
 * @property string $no_pol
 * @property Carbon $tanggal_mati_nopol
 * @property string $penyewa
 * @property Carbon $tanggal
 * @property int $nilai_awal
 * @property string $muatan
 * @property string $asal
 * @property string $tujuan
 * @property string $keterangan
 * @property int $flag
 * @property int $users_id
 * @property int $approve
 * @property int $truck_id
 * @property int $equipment_id
 * @property int $company_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property User $user
 * @property Collection|DataInvoiceDetail[] $data_invoice_details
 *
 * @package App\Models
 */
class DataMuatan extends Model
{
	use SoftDeletes;
	protected $table = 'data_muatan';

	protected $casts = [
		'jumlah' => 'int',
		'nilai_awal' => 'int',
		'flag' => 'int',
		'users_id' => 'int',
        'approve' => 'int',
        'truck_id' => 'int',
        'equipment_id' => 'int',
        'company_id' => 'int'
	];

	protected $dates = [
		'tanggal_mati_nopol',
		'tanggal'
	];

	protected $fillable = [
		'kode_data',
		'no_trip',
		'penerima',
		'jumlah',
		'jenis_biaya',
		'no_pol',
		'tanggal_mati_nopol',
		'penyewa',
		'tanggal',
		'nilai_awal',
		'muatan',
		'asal',
		'tujuan',
		'keterangan',
		'flag',
		'users_id',
        'approve',
        'truck_id',
        'equipment_id',
        'company_id'
	];

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
    public function truck()
    {
        return $this->belongsTo(Truck::class, 'truck_id');
    }
    public function equipment()
    {
        return $this->belongsTo(Equipment::class, 'equipment_id');
    }
    public function company()
    {
        return $this->belongsTo(Truck::class, 'company_id');
    }

	public function data_invoice_details()
	{
		return $this->hasMany(DataInvoiceDetail::class);
	}
}
