<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Modul
 *
 * @property int $id
 * @property string $modul
 * @property string $url
 * @property int $parent_id
 * @property int $is_hidden
 * @property string $icon_class
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 */
class Modul extends Model
{
    use SoftDeletes;

    protected $table = 'modul';

    protected $casts = [
        'id' => 'int',
        'parent_id' => 'int',
        'is_hidden' => 'int'
    ];

    protected $fillable = [
        'id',
        'modul',
        'url',
        'parent_id',
        'is_hidden',
        'icon_class'
    ];

    public $appends = ['modul_id', 'text', 'spriteCssClass'];

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'roles_modul', 'modul_id', 'roles_id')
            ->withPivot('id', 'parent_id', 'is_hidden');
    }

    public function modul_access()
    {
        return $this->hasMany(ModulAccess::class, 'modul_id');
    }

    function getSpriteCssClassAttribute()
    {
        return $this->icon_class;
    }

    function getModulIdAttribute()
    {
        return $this->id;
    }

    function getTextAttribute()
    {
        return $this->modul . " (" . $this->url . ")";
    }
}
