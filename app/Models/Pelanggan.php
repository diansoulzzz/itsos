<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Controllers\Admin\Tools\Select2;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pelanggan
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * @property string $nama_1
 * @property string $nama_2
 * @property Carbon $tgl_lahir_1
 * @property Carbon $tgl_lahir_2
 *
 * @property Collection|Acara[] $acaras
 * @property Collection|OrderKerja[] $order_kerjas
 *
 * @package App\Models
 */
class Pelanggan extends Model
{
	use SoftDeletes;
	protected $table = 'pelanggan';

	protected $dates = [
		'tgl_lahir_1',
		'tgl_lahir_2'
	];

	protected $fillable = [
		'kode',
		'nama',
		'nama_1',
		'nama_2',
		'tgl_lahir_1',
		'tgl_lahir_2'
	];

	public function acaras()
	{
		return $this->hasMany(Acara::class);
	}

	public function order_kerjas()
	{
		return $this->hasMany(OrderKerja::class);
	}
    public $appends = ['s2_value', 's2_text'];

    function getS2ValueAttribute()
    {
        return Select2::genS2Value($this->id, $this->s2_text);
    }

    function getS2TextAttribute()
    {
        return '(' . $this->kode . ') ' . $this->nama;
    }
}
