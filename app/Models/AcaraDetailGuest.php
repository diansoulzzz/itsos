<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AcaraDetailGuest
 *
 * @property int $id
 * @property int $acara_id
 * @property string $email
 * @property string $nama
 * @property string $no_telp
 * @property int $is_vip
 * @property int $is_hadir
 * @property string $qrcode
 *
 * @property Acara $acara
 *
 * @package App\Models
 */
class AcaraDetailGuest extends Model
{
    protected $table = 'acara_detail_guest';
    public $timestamps = false;

    protected $casts = [
        'acara_id' => 'int',
        'is_vip' => 'int',
        'is_hadir' => 'int'
    ];

    protected $fillable = [
        'acara_id',
        'email',
        'nama',
        'no_telp',
        'is_vip',
        'is_hadir',
        'qrcode',
        'email_at'
    ];

    public function acara()
    {
        return $this->belongsTo(Acara::class);
    }
}
