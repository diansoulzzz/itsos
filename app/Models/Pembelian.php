<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pembelian
 *
 * @property int $id
 * @property int $kode_nota
 * @property Carbon $tgl
 * @property int $supplier_id
 * @property float $grand_total
 * @property string $keterangan
 * @property int $updated_by
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Supplier $supplier
 * @property User $user
 * @property Collection|PembelianDetail[] $pembelian_details
 *
 * @package App\Models
 */
class Pembelian extends Model
{
	use SoftDeletes;
	protected $table = 'pembelian';

	protected $casts = [
		'supplier_id' => 'int',
		'grand_total' => 'float',
		'updated_by' => 'int',
		'created_by' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'kode_nota',
		'tgl',
		'supplier_id',
		'grand_total',
		'keterangan',
		'updated_by',
		'created_by'
	];

	public function supplier()
	{
		return $this->belongsTo(Supplier::class,'supplier_id');
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'updated_by');
	}

	public function pembelian_details()
	{
		return $this->hasMany(PembelianDetail::class);
	}
}
