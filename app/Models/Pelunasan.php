<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Pelunasan
 *
 * @property int $id
 * @property int $data_invoice_header_id
 * @property string $no_kwitansi
 * @property int $jumlah_tagihan
 * @property string $penyewa
 * @property Carbon $tanggal_bayar
 * @property int $nominal_dibayar
 * @property string $bank
 * @property int $audit
 * @property string $status
 * @property int $users_id
 *
 * @property DataInvoiceHeader $data_invoice_header
 * @property User $user
 *
 * @package App\Models
 */
class Pelunasan extends Model
{
	protected $table = 'pelunasan';
	public $timestamps = false;

	protected $casts = [
		'data_invoice_header_id' => 'int',
		'jumlah_tagihan' => 'int',
		'nominal_dibayar' => 'int',
		'audit' => 'int',
		'users_id' => 'int'
	];

	protected $dates = [
		'tanggal_bayar'
	];

	protected $fillable = [
		'data_invoice_header_id',
		'no_kwitansi',
		'jumlah_tagihan',
		'penyewa',
		'tanggal_bayar',
		'nominal_dibayar',
		'bank',
		'audit',
		'status',
		'users_id'
	];

	public function data_invoice_header()
	{
		return $this->belongsTo(DataInvoiceHeader::class);
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
