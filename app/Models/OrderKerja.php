<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrderKerja
 *
 * @property int $id
 * @property string $kode_nota
 * @property Carbon $tgl
 * @property string $masalah
 * @property int $pelanggan_id
 * @property int $mekanik_id
 * @property string $status
 * @property string $no_plat
 * @property string $no_rangka
 * @property string $no_lambung
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Mekanik $mekanik
 * @property Pelanggan $pelanggan
 * @property Collection|OrderKerjaFoto[] $order_kerja_fotos
 *
 * @package App\Models
 */
class OrderKerja extends Model
{
	use SoftDeletes;
	protected $table = 'order_kerja';

	protected $casts = [
		'pelanggan_id' => 'int',
		'mekanik_id' => 'int'
	];

	protected $dates = [
		'tgl'
	];

	protected $fillable = [
		'kode_nota',
		'tgl',
		'masalah',
		'pelanggan_id',
		'mekanik_id',
		'status',
		'no_plat',
		'no_rangka',
		'no_lambung'
	];

	public function mekanik()
	{
		return $this->belongsTo(Mekanik::class);
	}

	public function pelanggan()
	{
		return $this->belongsTo(Pelanggan::class);
	}

	public function order_kerja_fotos()
	{
		return $this->hasMany(OrderKerjaFoto::class);
	}

    public function order_kerja_barangs()
    {
        return $this->hasMany(OrderKerjaBarang::class,'');
    }
}
