<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Controllers\Admin\Tools\Select2;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Supplier
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|Pembelian[] $pembelians
 *
 * @package App\Models
 */
class Supplier extends Model
{
	use SoftDeletes;
	protected $table = 'supplier';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int'
	];

	protected $fillable = [
		'kode',
		'nama'
	];

	public function pembelians()
	{
		return $this->hasMany(Pembelian::class);
	}

    public $appends = ['s2_value', 's2_text'];

    function getS2ValueAttribute()
    {
        return Select2::genS2Value($this->id, $this->s2_text);
    }

    function getS2TextAttribute()
    {
        return '(' . $this->kode . ') ' . $this->nama;
    }
}
