<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Atlet
 *
 * @property int $id
 * @property int $users_id
 * @property int $klub_id
 * @property int $kelas_id
 * @property int $sabuk_id
 * @property int $pelatih_id
 * @property string $nama_lengkap
 * @property Carbon $tgl_lahir
 * @property string $gender
 * @property int $berat
 * @property int $tinggi
 * @property int $aktif
 * @property int $banned
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Atlet $atlet
 * @property Kela $kela
 * @property Klub $klub
 * @property Sabuk $sabuk
 * @property User $user
 * @property Collection|Atlet[] $atlets
 * @property Collection|KategoriAcara[] $kategori_acaras
 *
 * @package App\Models
 */
class Atlet extends Model
{
	use SoftDeletes;
	protected $table = 'atlet';

	protected $casts = [
		'users_id' => 'int',
		'klub_id' => 'int',
		'kelas_id' => 'int',
		'sabuk_id' => 'int',
		'pelatih_id' => 'int',
		'berat' => 'int',
		'tinggi' => 'int',
		'aktif' => 'int',
		'banned' => 'int'
	];

	protected $dates = [
		'tgl_lahir'
	];

	protected $fillable = [
		'users_id',
		'klub_id',
		'kelas_id',
		'sabuk_id',
		'pelatih_id',
		'nama_lengkap',
		'tgl_lahir',
		'gender',
		'berat',
		'tinggi',
		'aktif',
		'banned'
	];

	public function atlet()
	{
		return $this->belongsTo(Atlet::class, 'pelatih_id');
	}

	public function kela()
	{
		return $this->belongsTo(Kela::class, 'kelas_id');
	}

	public function klub()
	{
		return $this->belongsTo(Klub::class);
	}

	public function sabuk()
	{
		return $this->belongsTo(Sabuk::class);
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}

	public function atlets()
	{
		return $this->hasMany(Atlet::class, 'pelatih_id');
	}

	public function kategori_acaras()
	{
		return $this->hasMany(KategoriAcara::class);
	}
}
