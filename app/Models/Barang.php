<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Controllers\Admin\Tools\Select2;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Barang
 *
 * @property int $id
 * @property string $kode
 * @property string $nama
 * @property int $barang_kategori_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property BarangKategori $barang_kategori
 * @property Collection|PembelianDetail[] $pembelian_details
 *
 * @package App\Models
 */
class Barang extends Model
{
    use SoftDeletes;

    protected $table = 'barang';

    protected $casts = [
        'barang_kategori_id' => 'int'
    ];

    protected $fillable = [
        'kode',
        'nama',
        'barang_kategori_id'
    ];

    public function barang_kategori()
    {
        return $this->belongsTo(BarangKategori::class);
    }

    public function pembelian_details()
    {
        return $this->hasMany(PembelianDetail::class);
    }
    public function barang_satuans()
    {
        return $this->hasMany(Satuan::class);
    }

    public $appends = ['s2_value', 's2_text'];

    function getS2ValueAttribute()
    {
        return Select2::genS2Value($this->id, $this->s2_text);
    }

    function getS2TextAttribute()
    {
        return '(' . $this->kode . ') ' . $this->nama;
    }
}
