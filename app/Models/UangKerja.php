<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class UangKerja
 *
 * @property int $id
 * @property string $no_nota
 * @property int $equipment_header_id
 * @property string $penyewa
 * @property Carbon $tanggal
 * @property int $company_id
 * @property int $users_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Company $company
 * @property Equipment $equipment
 * @property User $user
 * @property Collection|UangKerjaDetail[] $uang_kerja_details
 *
 * @package App\Models
 */
class UangKerja extends Model
{
	use SoftDeletes;
	protected $table = 'uang_kerja';

	protected $casts = [
		'equipment_header_id' => 'int',
		'company_id' => 'int',
		'users_id' => 'int'
	];

	protected $dates = [
		'tanggal'
	];

	protected $fillable = [
		'no_nota',
		'equipment_header_id',
		'penyewa',
		'tanggal',
		'company_id',
		'users_id'
	];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function equipment()
	{
		return $this->belongsTo(Equipment::class, 'equipment_header_id');
	}

	public function users()
	{
		return $this->belongsTo(User::class, 'users_id');
	}

	public function uang_kerja_details()
	{
		return $this->hasMany(UangKerjaDetail::class, 'uang_kerja_header_id');
	}
}
