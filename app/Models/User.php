<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Helpers\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

/**
 * Class User
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $nama
 * @property string $api_token
 * @property int $roles_id
 * @property int $active_company_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Role $role
 * @property Collection|DataInvoiceHeader[] $data_invoice_headers
 * @property Collection|DataInvoicePrint[] $data_invoice_prints
 * @property Collection|DataMuatan[] $data_muatans
 * @property Collection|Pelunasan[] $pelunasans
 *
 * @package App\Models
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    protected $table = 'users';

    protected $casts = [
        'roles_id' => 'int',
        'active_company_id' => 'int'
    ];

    protected $hidden = [
        'password'
    ];

    protected $fillable = [
        'username',
        'password',
        'nama',
        'roles_id',
        'active_company_id',
        'api_token'
    ];

    protected $appends = ['email', 'name', 'inisial', 'count_company_id'];

    function getCountCompanyIdAttribute()
    {
        return count($this->users_company);
    }

    function getNameAttribute()
    {
        return $this->nama;
    }

    function getEmailAttribute()
    {
        return $this->username;
    }

    public function getInisialAttribute()
    {
        if (isset($this->nama)) {
            return Helper::strChar($this->nama);
        }
        return Helper::strChar("No Name");
    }

    public function role()
    {
        return $this->belongsTo(Role::class, 'roles_id');
    }

    public function data_invoice_headers()
    {
        return $this->hasMany(DataInvoiceHeader::class, 'users_id');
    }

    public function data_invoice_prints()
    {
        return $this->hasMany(DataInvoicePrint::class, 'users_id');
    }

    public function data_muatans()
    {
        return $this->hasMany(DataMuatan::class, 'users_id');
    }

    public function pelunasans()
    {
        return $this->hasMany(Pelunasan::class, 'users_id');
    }

    public function users_company()
    {
        return $this->belongsToMany(Company::class, 'users_company', 'users_id', 'company_id')
            ->withPivot('id');
    }

    public function active_company()
    {
        return $this->belongsTo(Company::class, 'active_company_id');
    }
}
