<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * 
 * @property int $id
 * @property string $model_series
 * @property Carbon $tanggal
 * @property string $perawatan
 * @property string $status
 * @property Carbon $updated_at
 * @property Carbon $created_at
 * @property string $deleted_at
 * @property int $company_id
 * 
 * @property Company $company
 * @property Collection|DataMuatan[] $data_muatans
 * @property Collection|EquipmentDetail[] $equipment_details
 * @property Collection|UangKerja[] $uang_kerjas
 *
 * @package App\Models
 */
class Equipment extends Model
{
	use SoftDeletes;
	protected $table = 'equipment';

	protected $casts = [
		'company_id' => 'int'
	];

	protected $dates = [
		'tanggal'
	];

	protected $fillable = [
		'model_series',
		'tanggal',
		'perawatan',
		'status',
		'company_id'
	];

	public function company()
	{
		return $this->belongsTo(Company::class);
	}

	public function data_muatans()
	{
		return $this->hasMany(DataMuatan::class);
	}

	public function equipment_details()
	{
		return $this->hasMany(EquipmentDetail::class, 'equipment_header_id');
	}

	public function uang_kerjas()
	{
		return $this->hasMany(UangKerja::class, 'equipment_header_id');
	}
}
