<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Report
 *
 * @property int $id
 * @property string $code
 * @property string $type
 * @property string $name
 * @property string $query
 * @property string $modul_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @package App\Models
 */
class Report extends Model
{
    use SoftDeletes;

    protected $table = 'report';

    protected $casts = [
        'modul_id' => 'int',
    ];

    protected $fillable = [
        'code',
        'type',
        'name',
        'query'
    ];
    public function modul()
    {
        return $this->belongsTo(Modul::class,'modul_id');
    }
//    public $appends = ['modul_id', 'parent_id', 'text', 'spriteCssClass'];
//
//    function getSpriteCssClassAttribute()
//    {
//        return null;
//    }
//
//    function getModulIdAttribute()
//    {
//        return "R" . $this->id;
//    }
//
//    function getParentIdAttribute()
//    {
////        switch ($this->type) {
////            case "PENJUALAN":
////                return 1;
////        }
//        return 5;
//    }
//
//    function getTextAttribute()
//    {
//        return $this->code . " (" . $this->name . ")";
//    }
}
