<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use App\Http\Controllers\Admin\Tools\Select2;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class BarangKategori
 *
 * @property int $id
 * @property string $nama
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 *
 * @property Collection|Barang[] $barangs
 *
 * @package App\Models
 */
class BarangKategori extends Model
{
    use SoftDeletes;

    protected $table = 'barang_kategori';

    protected $fillable = [
        'nama'
    ];

    public function barangs()
    {
        return $this->hasMany(Barang::class);
    }

    public $appends = ['s2_value', 's2_text'];

    function getS2ValueAttribute()
    {
        return Select2::genS2Value($this->id, $this->s2_text);
    }

    function getS2TextAttribute()
    {
        return '(' . $this->kode . ') ' . $this->nama;
    }
}
