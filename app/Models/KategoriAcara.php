<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class KategoriAcara
 * 
 * @property int $id
 * @property int $kategori_id
 * @property int $acara_id
 * @property int $atlet_id
 * @property int $kelas_id
 * @property int $juara_id
 * @property int $score
 * @property int $pengkot_id
 * 
 * @property Acara $acara
 * @property Atlet $atlet
 * @property Juara $juara
 * @property Kategori $kategori
 * @property Kela $kela
 * @property Pengkot $pengkot
 *
 * @package App\Models
 */
class KategoriAcara extends Model
{
	protected $table = 'kategori_acara';
	public $timestamps = false;

	protected $casts = [
		'kategori_id' => 'int',
		'acara_id' => 'int',
		'atlet_id' => 'int',
		'kelas_id' => 'int',
		'juara_id' => 'int',
		'score' => 'int',
		'pengkot_id' => 'int'
	];

	protected $fillable = [
		'kategori_id',
		'acara_id',
		'atlet_id',
		'kelas_id',
		'juara_id',
		'score',
		'pengkot_id'
	];

	public function acara()
	{
		return $this->belongsTo(Acara::class);
	}

	public function atlet()
	{
		return $this->belongsTo(Atlet::class);
	}

	public function juara()
	{
		return $this->belongsTo(Juara::class);
	}

	public function kategori()
	{
		return $this->belongsTo(Kategori::class);
	}

	public function kela()
	{
		return $this->belongsTo(Kela::class, 'kelas_id');
	}

	public function pengkot()
	{
		return $this->belongsTo(Pengkot::class);
	}
}
