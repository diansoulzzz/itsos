<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Pengkot
 * 
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property int $aktif
 * @property int $banned
 * @property string $telpon
 * @property string $keterangan
 * @property int $users_id
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string $deleted_at
 * 
 * @property User $user
 *
 * @package App\Models
 */
class Pengkot extends Model
{
	use SoftDeletes;
	protected $table = 'pengkot';

	protected $casts = [
		'aktif' => 'int',
		'banned' => 'int',
		'users_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'alamat',
		'aktif',
		'banned',
		'telpon',
		'keterangan',
		'users_id'
	];

	public function user()
	{
		return $this->belongsTo(User::class, 'users_id');
	}
}
