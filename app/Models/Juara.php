<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Juara
 * 
 * @property int $id
 * @property string $nama
 * @property int $poin
 * 
 * @property Collection|KategoriAcara[] $kategori_acaras
 *
 * @package App\Models
 */
class Juara extends Model
{
	protected $table = 'juara';
	public $timestamps = false;

	protected $casts = [
		'poin' => 'int'
	];

	protected $fillable = [
		'nama',
		'poin'
	];

	public function kategori_acaras()
	{
		return $this->hasMany(KategoriAcara::class);
	}
}
