<?php

namespace App\Helpers;

use App\Constants\Constant;
use App\Services\FileUploadProcessor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Chart
{
    protected $queryString;
    protected $request;
    protected $connection = Constant::DefConnection;
    protected $type;
    protected $title = '';
    protected $subtitle = '';
    protected $xTitle = '';
    protected $yTitle = '';
    protected $report;

    public function __construct(Request $request, $queryString, $type)
    {
        $this->request = $request;
        $this->queryString = $queryString;
        $this->type = $type;
        $this->report = new Report($this->request, $this->queryString);
    }

    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
        return $this;
    }

    public function setXTitle($xTitle)
    {
        $this->xTitle = $xTitle;
        return $this;
    }

    public function setXYitle($yTitle)
    {
        $this->yTitle = $yTitle;
        return $this;
    }

    public function get()
    {
        $queryResult = $this->report->toSql();
        $data = collect(DB::connection($this->connection)->select($queryResult));
        $collection = collect($data);
        $categories = $collection->pluck('categories');
        $seriesKey = [];
        foreach ($collection as $key => $val) {
            $seriesKey[$val->series][] = $val->value;
        }
        $series = [];
        foreach ($seriesKey as $key => $val) {
            $series[] = [
                'name' => ucwords($key),
                'data' => $val
            ];
        }
        $chart = [
            'type' => strtolower($this->type),
            'height' => 'inherit',
//            'width' => 'inherit',
        ];
        $title = [
            'text' => $this->title,
        ];
        $subtitle = [
            'text' => $this->subtitle
        ];
        $yAxis = [
            'min' => 0,
            'title' => [
                'text' => $this->yTitle
            ]
        ];
        $xAxis = [
            'min' => 0,
            'title' => [
                'text' => $this->xTitle
            ]
        ];
        if ($categories) {
            $yAxis['categories'] = $categories;
        }
        $legend = [
            'layout' => 'vertical',
            'align' => 'right',
            'verticalAlign' => 'middle'
        ];
        $plotOptions = [
            'series' => [
                'label' => [
                    'connectorAllowed' => false,
                ],
                'pointStart' => 2010,
            ]
        ];
        $exporting = [
            'buttons' => [
                'contextButton' => [
                    'menuItems' => ['downloadPNG', 'downloadJPEG', 'downloadPDF', 'downloadSVG']
                ]
            ]
        ];
        $responsive = [
            'rules' => [
                [
                    'condition' => [
                        'maxWidth' => 500
                    ],
                    'chartOptions' => [
                        'legend' => [
                            'layout' => 'horizontal',
                            'align' => 'center',
                            'verticalAlign' => 'bottom',
                        ]
                    ]
                ]
            ]
        ];
        return [
            'chart' => $chart,
            'title' => $title,
            'subtitle' => $subtitle,
            'yAxis' => $yAxis,
            'xAxis' => $xAxis,
            'legend' => $legend,
            'plotOptions' => $plotOptions,
            'responsive' => $responsive,
            'series' => $series,
            'exporting' => $exporting,
        ];
    }
}
