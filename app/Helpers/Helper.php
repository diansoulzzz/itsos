<?php

namespace App\Helpers;

use App\Constants\Constant;
use App\Models\Company;
use App\Models\Modul;
use App\Models\ModulAccess;
use App\Models\RolesModulAccess;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Helper
{
    public static function getResponse($data = [], $message = 'OK', $responseType = Constant::ResponseSuccess, $statusCode = 200)
    {
        $result = [];
        if ($responseType == Constant::ResponseSuccess) {
            $result['data'] = $data;
            $result['message'] = $message;
        }
        if ($responseType == Constant::ResponseError) {
            $result['errors'] = (is_string($message) ? [$message] : $message);
            $statusCode = 500;
        }
        return response($result);
    }


    public static function toArray($collection)
    {
        return json_decode(json_encode($collection), true);
    }

    public static function toStd($array)
    {
        if (is_array($array)) {
            return (object)array_map(self::toStd($array), $array);
        } else {
            return $array;
        }
    }

    public static function getTransNo($inisial, $trans_date, $model, $column)
    {
        $prefix = self::getPrefixNo($inisial, $trans_date);
        $last_trans = $model::where($column, 'like', $prefix . '%')->orderByDesc($column)->withTrashed()->first();
        $no = 1;
        if ($last_trans) {
            $last_trans->toArray();
            $no = substr($last_trans[$column], strlen($prefix), 6) + 1;
        }
        return $prefix . sprintf('%06d', $no);
    }

    public static function strposAll($haystack, $needle, $split_by)
    {
        $offset = 0;
        $allPos = [];
        $arrPos = [];
        $loop = 0;
        while (($pos = strpos($haystack, $needle, $offset)) !== FALSE) {
            $offset = $pos + 1;
            $arrPos[] = $pos;
            if ($split_by > 0) {
                if ($loop % $split_by) {
                    $allPos[] = $arrPos;
                    $arrPos = [];
                }
            } else {
                $allPos[] = $pos;
            }
            $loop++;
        }
        return $allPos;
    }

    public static function getPrefixNo($inisial, $trans_date)
    {
        $cabang = auth()->user()->active_company->cabang;
//        $cabang = Company::first('cabang')->cabang;
        $dd = $trans_date->format('d');
        $mm = $trans_date->format('m');
        $yy = $trans_date->format('y');
        return $cabang . '/' . $inisial . '/' . $yy . $mm . '/' . $dd . '/';
    }

    public static function strChar(string $string)
    {
        $words = preg_split("/\s+/", $string);
        $res = '';
        foreach ($words as $word) {
            $res .= ucfirst($word[0]);
        }
        return substr($res, 0, 2);
    }

    public static function encode($data)
    {
        if (empty($data)) {
            return '';
        }
        return json_encode($data);
    }

    public static function isJson($str)
    {
        $result = false;
        if (!preg_match("/^\d+$/", trim($str))) {
            json_decode($str);
            $result = (json_last_error() == JSON_ERROR_NONE);
        }
        return $result;
    }

    public static function merge(Request $request)
    {
        $data = self::decodeAll($request->all());
        $req = $request->duplicate();
        return $req->merge($data);
    }

    public static function decodeAll($datas)
    {
        $result = [];
        foreach ($datas as $i => $value) {
            if (is_array($value)) {
                $result[$i] = self::decodeAll($value);
                continue;
            }
            if (self::isJson($value)) {
                $result[$i] = json_decode($value);
                continue;
            }
            $result[$i] = $value;
        }
        return $result;
    }

    public static function s2($q, $model)
    {
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            if (is_array($model)) {
                $items = $model;
                $total_count = count($model);
            } else {
                $items = $model->get();
                $total_count = $items->count();
            }
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public static function myError($message)
    {
        $message = trim(preg_replace('/\s+/', ' ', $message));
        $needle_start = '<<Unknown error>>';
        $needle_end_ctx = '(SQL: update';
        $needle_end_det = 'DETAIL: ';
        $needle_abort = "transaction is aborted";
        if (strpos($message, $needle_abort) > 1) {
            return "There's an error when processing your query.";
        }
        $pos_start = strpos($message, $needle_start) + strlen($needle_start);
        $pos_end_ctx = strpos($message, $needle_end_ctx);
        $pos_end_det = strpos($message, $needle_end_det);
        if ($pos_end_ctx > 0) {
            $pos_end = $pos_end_ctx - $pos_start - 1;
        } else if ($pos_end_det > 0) {
            $pos_end = $pos_end_det - $pos_start - 1;
        } else {
            $pos_end = strlen($message);
        }
        $message = substr($message, $pos_start, $pos_end);
        return str_replace('"', "`", $message);
    }

    public static function pgError($message)
    {
        $message = trim(preg_replace('/\s+/', ' ', $message));
        $needle_start = 'ERROR: ';
        $needle_end_ctx = 'CONTEXT: ';
        $needle_end_det = 'DETAIL: ';
        $needle_abort = "transaction is aborted";
        if (strpos($message, $needle_abort) > 1) {
            return "There's an error when processing your query.";
        }
        $pos_start = strpos($message, $needle_start) + strlen($needle_start);
        $pos_end_ctx = strpos($message, $needle_end_ctx);
        $pos_end_det = strpos($message, $needle_end_det);
        if ($pos_end_ctx > 0) {
            $pos_end = $pos_end_ctx - $pos_start - 1;
        } else if ($pos_end_det > 0) {
            $pos_end = $pos_end_det - $pos_start - 1;
        } else {
            $pos_end = strlen($message);
        }
        $message = substr($message, $pos_start, $pos_end);
        return str_replace('"', "`", $message);
    }

    public static function redirect($route, $alert, $title, $message, $params = [])
    {
        if (!$route) {
//            $message = $message;
            if ($alert === Constant::AlertDanger) {
                $message = self::myError($message);
                $message = 'Terjadi Kesalahan. ' . $message;
            }
            return redirect()->back()->withInput()->withStatus([
                'alert' => $alert,
                'status' => $title,
                'message' => $message,
            ]);
        }
        return redirect()->route($route, $params)->withStatus([
            'alert' => $alert,
            'status' => $title,
            'message' => $message,
        ]);
    }

    public static function selectMany($query, $connection = Constant::DefConnection)
    {
        $query = rtrim($query, ";");
        $querys = explode(';', $query);
        $res = '';
        foreach ($querys as $i => $q) {
            if (count($querys) - 1 == $i) {
                $res = DB::connection($connection)->select(DB::raw($q));
                break;
            }
            DB::connection($connection)->select(DB::raw($q));
        }
        return $res;
    }

    public static function PDOquery($query, $connection = Constant::DefConnection)
    {
        $db = DB::connection($connection)->getPdo();
        $query = rtrim($query, ";");
        $querys = explode(';', $query);
        $res = '';
        foreach ($querys as $i => $q) {
            if (count($querys) - 1 == $i) {
                $res = $db->query($q);
//                $res = DB::connection($connection)->select(DB::raw($q));
                break;
            }
            $res = $db->query($q);
//            DB::connection($connection)->select(DB::raw($q));
        }
        return $res;
    }


    public static function hasAccess($moduls, $url)
    {
        $collection = collect($moduls);
        return $collection->contains('url', $url);
    }

    public static function genSidebar(Request $request, $trees)
    {
        $result = '';
        foreach ($trees as $i => $value) {
            if ($value['is_hidden']) continue;
            $result .= '<li class="kt-menu__item ' . $request->is($value['modul']) . '"
                        aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
                        <a href="' . ($value['url'] != "#" ? ($value['url'] == "report" ? route($value['url'], ['i' => $value['id']]) : route($value['url'])) : "javascript:void(0);") . '" class="kt-menu__link kt-menu__toggle">';
            if ($value['icon_class']) {
                $result .= '<span class="kt-menu__link-icon">
                            <i class="' . $value['icon_class'] . '" aria-hidden="true"></i>
                        </span>';
            }
            $result .= '<span class="kt-menu__link-text">' . $value['modul'] . '</span>';
            if ($value['items']) {
                $result .= '<i class="kt-menu__ver-arrow la la-angle-right"></i>';
            }
            $result .= '</a>';
            if ($value['items']) {
                $result .= '<div class="kt-menu__submenu ">
                            <span class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">';
                $result .= self::genSidebar($request, $value['items']);
                $result .= '</ul></div>';
            }
            $result .= '</li>';
        }
        return $result;
    }

    public static function ungroupTreeMenu($menus = null, $parent = null)
    {
        $result = [];
        if (!is_array($menus)) {
            $menus = func_get_args();
        }
        foreach ($menus as $i => $val) {
            $val = collect($val)->toArray();
            if (isset($val['items']) && (count($val['items']))) {
                $result = array_merge($result, self::ungroupTreeMenu($val['items'], $val['modul_id']));
            }
            unset($val['items']);
            unset($val['pivot']);
            $val['parent_id'] = $parent;
            $result = array_merge($result, [$i => $val]);
        }
        return $result;
    }

    public static function groupTreeMenu($menus)
    {
        $results = [];
        $children = [];
        foreach ($menus as $val) {
            if (isset($val['pivot'])) {
                $i_id = $val['pivot']['modul_id'];
                $p_id = $val['pivot']['parent_id'];
            } else {
                $i_id = $val['modul_id'];
                $p_id = $val['parent_id'];
            }

            if (!isset($children[$i_id])) {
                $children[$i_id] = [];
            }
            $val['items'] = &$children[$i_id];
            if (!strlen($p_id)) {
                $results[] = $val;
            } else {
                if (!isset($children[$p_id])) {
                    $children[$p_id] = [];
                }
                $children[$p_id][] = $val;
            }
        }
        return $results;
    }

    public static function sidebar(Request $request, $moduls)
    {
        $trees = self::groupTreeMenu($moduls);
        return self::genSidebar($request, $trees);
    }

    public static function checkAccess($request, $stringAccess)
    {
        return self::checkAccessUrl($request->route()->getName(), $stringAccess);
    }

    public static function checkAccessUrl($url, $stringAccess)
    {
        $roles_id = auth()->user()->roles_id;
        $modul = Modul::where('url', $url)->first();
        $modul_id = $modul->id;
        $query = "select count(*) as Granted
        from modul m,
             modul_access ma,
             roles_modul_access rma
        where m.id = ma.modul_id
            and ma.id = rma.modul_access_id
            and m.id ='" . $modul_id . "'
            and rma.roles_id = '" . $roles_id . "'
            and ma.access = '" . $stringAccess . "'
        limit 1;";
        return collect(DB::select($query))->first()->Granted;
    }

    public static function checkAccessModul($modul, $stringAccess)
    {
        $roles_id = auth()->user()->roles_id;
        $modul = Modul::where('url', $modul)->first();
        $modul_id = $modul->id;
        $query = "select count(*) as Granted
        from modul m,
             modul_access ma,
             roles_modul_access rma
        where m.id = ma.modul_id
            and ma.id = rma.modul_access_id
            and m.id ='" . $modul_id . "'
            and rma.roles_id = '" . $roles_id . "'
            and ma.access = '" . $stringAccess . "'
        limit 1;";
        return collect(DB::select($query))->first()->Granted;
    }

    public static function syncMany(HasMany $hasMany, $values, $key)
    {
        $relations = clone $hasMany;
        $values = collect($values);
        $inserts = [];
        (clone $relations)->whereNotIn($key, $values->pluck($key)->filter())->delete();
        foreach ($values as $val) {
            if ((clone $relations)->where($key, '=', $val[$key])->exists()) {
                (clone $relations)->where($key, '=', $val[$key])->update($val);
            } else {
                $inserts[] = collect($val)->except($key)->toArray();
            }
        }
        (clone $relations)->createMany($inserts);
    }

}
