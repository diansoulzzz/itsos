<?php

namespace App\Helpers;

use App\Constants\Constant;
use App\Services\FileUploadProcessor;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MongoDB\BSON\Type;

class Select
{
    const TypeModel = 'Model';
    const TypeQuery = 'Query';
    const DefColID = 'id';
    const DefColText = 'text';

    protected $keywords;
    protected $connection = Constant::DefConnection;
    protected $type;
    protected $queryString;
    protected $model;
    protected $modelColumn;
    protected $modelWhere;
    protected $withAll;
    protected $items = [];
    protected $limit = 1;
    protected $columnId = self::DefColID;
    protected $columnText = self::DefColText;
    protected $isUnique = false;
    protected $extra = [];

    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }

    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }

    public function setColumnId($columnId)
    {
        $this->columnId = $columnId;
        return $this;
    }

    public function setColumnText($columnText)
    {
        $this->columnText = $columnText;
        return $this;
    }

    public function setUnique($isUnique)
    {
        $this->isUnique = $isUnique;
        return $this;
    }

    public function setModel($model, $modelColumn, $modelWhere = [], $columnId = self::DefColID, $columnText = self::DefColText)
    {
        $this->type = self::TypeModel;
        $this->model = $model;
        $this->modelColumn = $modelColumn;
        $this->modelWhere = $modelWhere;
        $this->columnId = $columnId;
        $this->columnText = $columnText;
        return $this;
    }

    public function setSql($queryString, $columnId = self::DefColID, $columnText = self::DefColText)
    {
        $this->type = self::TypeQuery;
        $this->queryString = $queryString;
        $this->columnId = $columnId;
        $this->columnText = $columnText;
        return $this;
    }

    public function addExtra($keyName, $columnId, $columnText)
    {
        $this->extra[] = ['key' => $keyName, 'id' => $columnId, 'text' => $columnText];
        return $this;
    }

    public function withAll($bool)
    {
        if ($bool) {
            $all = [
                'id' => '%',
                'text' => 'ALL',
            ];
            $all = [
                'id' => json_encode($all),
                'text' => 'ALL'
            ];
            $this->withAll = $all;
        }
        return $this;
    }

    public function transMorph($items)
    {
        $result = [];
        $id = $this->columnId;
        $text = $this->columnText;
        foreach ($items as $item) {
            $withExtra = [];
            foreach ($this->extra as $extra) {
                $withExtra[$extra['key']] = $this->morph($item, $extra['id'], $extra['text']);
            }
            $res = $this->morph($item, $id, $text, $withExtra);
            $result[] = $res;
        }
        return $result;
    }

    protected function morph($item, $columnId, $columnText, $withExtra = [])
    {
        $result = [
            'id' => $item->$columnId,
            'text' => $item->$columnText,
        ];
        foreach ($withExtra as $key => $extra) {
            $result[$key] = $extra;
        }
        return [
            'id' => json_encode($result),
            'text' => $item->$columnText
        ];
    }

    protected function getFromModel()
    {
        $items = $this->model::where(function ($query) {
            $keywords = '%' . $this->keywords . '%';
            $isFirst = true;
            foreach ($this->modelColumn as $column) {
                if ($isFirst) {
                    $query->where($column, 'ilike', $keywords);
                    $isFirst = false;
                }
                $query->orwhere($column, 'ilike', $keywords);
            }
        });
        if ($this->modelWhere) {
            $items = $items->where([$this->modelWhere]);
        }
        if ($this->isUnique) {
            $items = $items->distinct();
        }
        return $items->get([$this->columnId, $this->columnText]);
    }

    protected function getFromQuery()
    {
        return DB::select(DB::raw($this->queryString));
    }

    public function get()
    {
        $items = [];
        if (strlen($this->keywords) > $this->limit) {
            if ($this->type == self::TypeModel) {
                $items = $this->getFromModel();
            }
            if ($this->type == self::TypeQuery) {
                $items = $this->getFromQuery();
            }
            if ($this->columnId != self::DefColID || $this->columnText != self::DefColText || $this->queryString) {
                $items = $this->transMorph($items);
            }
            if ($this->withAll) {
                $items = collect($items)->push($this->withAll);
            }
        }
        return [
            'total_count' => count($items),
            'items' => $items,
        ];
    }
}
