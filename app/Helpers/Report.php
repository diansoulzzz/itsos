<?php

namespace App\Helpers;

use App\Constants\Constant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Report
{
    protected $queryString;
    protected $request;

    public function __construct(Request $request, $queryString)
    {
        $this->request = $request;
        $this->queryString = $queryString;
    }

    public function getParam()
    {
        $allPos = Helper::strposAll($this->queryString, Constant::ParamSegment, 2);
        $strPos = [];
        foreach ($allPos as $i => $val) {
            $name = substr($this->queryString, $val[0], $val[1] - $val[0] + 1);
            $type = 'STRING';
            $value = '';
            $split = explode(Constant::ParamSeparator, substr($this->queryString, ($val[0] + 1), $val[1] - $val[0] + 1));
            $label = $split[0];
            if (strpos(strtoupper($name), 'STRING')) {
                $type = 'STRING';
                $value = '';
            }
            if (strpos(strtoupper($name), 'DATETIME')) {
                $type = 'DATETIME';
                $value = Carbon::now();
            }
            if (strpos(strtoupper($name), 'CHOICE')) {
                $type = 'CHOICE';
                $choicePos = Helper::strposAll($name, Constant::ParamChoiceSegment, 0);
                $choiceQuery = substr($name, ($choicePos[0] + 1), $choicePos[1] - ($choicePos[0] + 1));
                $value = Helper::toArray(Helper::selectMany($choiceQuery));
                foreach ($value as $key => $val) {
                    $value[$key]['selected'] = '';
                }
            }
            $strPos[$name] = ['name' => $name, 'label' => $label, 'type' => $type, 'value' => $value];
        }
        $params = [];
        foreach ($strPos as $pos) {
            $params[] = $pos;
        }
        foreach ($params as $key => $param) {
            if ($this->request->has($param['label'])) {
                $input_value = $this->request->input($param['label']);
                if ($param['type'] == 'CHOICE') {
                    foreach ($params[$key]['value'] as $key_val => $value) {
                        if ($value['id'] == $input_value) {
                            $params[$key]['value'][$key_val]['selected'] = 'selected';
                        } else {
                            $params[$key]['value'][$key_val]['selected'] = '';
                        }
                    }
                } else {
                    $params[$key]['value'] = $input_value;
                }
            }
        }
        return $params;
    }

    public function toSql()
    {
        $queryParam = self::getParam();
        foreach ($queryParam as $item => $value) {
            $defaultValue = $value['value'];
            if ($value['type'] == "CHOICE") {
                $defaultValue = collect($value['value'])->first()['label'];
            }
            $this->queryString = str_replace($value['name'], $defaultValue, $this->queryString);
            if ($this->request->has($value['label'])) {
                $this->queryString = str_replace($value['name'], $this->request->input($value['label']), $this->queryString);
            }
        }
        return $this->queryString;
    }
}
