<?php

namespace App\Helpers;

use App\Constants\Constant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Grid
{
    const TypeDialog = "dialog";
    const TypeUrl = "url";

    protected $queryString;
    protected $request;
    protected $connection = Constant::DefConnection;
    protected $checkBox = false;
    protected $groupBy = [];
    protected $actions = [];
    protected $action = null;
    protected $hyperLinks = [];
    protected $editColumns = [];
    protected $addColumns = [];
    protected $headerAttributes = [];
    protected $contextMenus = [];
    protected $hidden = [];
    protected $report;

    public function __construct(Request $request, $queryString)
    {
        $this->request = $request;
        $this->queryString = $queryString;
        $this->report = new Report($this->request, $this->queryString);
    }

    public function addAction($url, $field, $iconClass, $label = null, $title = null, $type = self::TypeUrl, callable $condition = null)
    {
        $template = '<a role="button" href="javascript:void(0);" data-url="' . $url . '" data-field-name="' . $field . '" ' . ($title ? 'data-title="' . $title . '"' : ' ') . ($type ? 'data-type="' . $type . '"' : ' ') . ' class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                    ' . ($label ? $label : '') . '<i class="' . $iconClass . '"></i>
                </a>';
        $actions = [
            'template' => $template,
            'condition' => $condition,
        ];
        $this->actions[] = $actions;
        return $this;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    public function setConnection($connection)
    {
        $this->connection = $connection;
        return $this;
    }

    public function withCheckBox(bool $checkBox)
    {
        $this->checkBox = $checkBox;
        return $this;
    }

    public function editColumn($field, callable $fn)
    {
        $this->editColumns[] = [
            'field' => $field,
            'template' => $fn
        ];
        return $this;
    }

    public function addColumn($field, callable $fn)
    {
        $this->addColumns[] = [
            'field' => $field,
            'template' => $fn,
            'type' => 'def'
        ];
        return $this;
    }

    public function addHyperLinkOn($field, $url, $inputName, $targetId)
    {
        $this->hyperLinks[] = [
            'url' => $url,
            'field' => $field,
            'input_name' => $inputName,
            'target_id' => $targetId,
        ];
        return $this;
    }

    public function setHeaderAttributes($field, $class, $style)
    {
        $this->headerAttributes[] = [
            'field' => $field,
            'style' => $style,
            'class' => $class,
        ];
        return $this;
    }

    public function withEscape($template)
    {
        return urldecode($template);
    }

    public function setHidden($hidden = [])
    {
        $this->hidden = $hidden;
        return $this;
    }

    public function getParam()
    {
        return $this->report->getParam();
    }

    public function get()
    {
        $queryResult = $this->report->toSql();
        $data = collect(Helper::selectMany($queryResult, $this->connection));
        $columnDefs = [];
        $rowAggregate = [];
        $schemaDefs = [];
        $groupDefs = [];
        $columnsIds = (object)[];
        if ($this->checkBox) {
            $columnDefs[] = [
                'title' => 'check_box',
                'selectable' => true,
                'width' => 50,
            ];
        }
        $rs = Helper::PDOquery($queryResult, $this->connection);
        for ($i = 0; $i < $rs->columnCount(); $i++) {
            $col = $rs->getColumnMeta($i);
            $val = $col['name'];
            $type = $col['native_type'];
            $columnDef = [
                'field' => $val,
                'title' => ucwords(str_replace('_', ' ', $val)),
                'aggregates' => ["sum", "count", "min", "max"],
//                'headerCell' => 'headerCell',
            ];
            $columnsIds->$val = '#:' . $val . '#';
            if (strtolower($val) == 'id') {
                $columnDef['hidden'] = true;
            }
            switch ($type) {
                case "TIMESTAMP":
                case "DATETIME":
                case "timestamptz":
                    $columnDef['format'] = "{0: yyyy-MM-dd HH:mm:ss}";
                    break;
                case 'LONG':
                case 'TINY':
                case 'int4':
                    $columnDef['format'] = "{0:n0}";
                    $data = $data->map(function ($item, $key) use ($val) {
                        $item->$val = intval($item->$val);
                        return $item;
                    });
                    break;
                case 'DOUBLE':
                case "NEWDECIMAL":
                case "float8":
                    $columnDef['format'] = "{0:n}";
                    $data = $data->map(function ($item, $key) use ($val) {
                        $item->$val = floatval($item->$val);
                        return $item;
                    });
                    break;
            }
            $rowAggregate[] = [
                'field' => $val, 'aggregate' => 'sum',
            ];
            $rowAggregate[] = [
                'field' => $val, 'aggregate' => 'min',
            ];
            $rowAggregate[] = [
                'field' => $val, 'aggregate' => 'max',
            ];
            $rowAggregate[] = [
                'field' => $val, 'aggregate' => 'count',
            ];
            $columnDefs[] = $columnDef;
        }

        if (count($this->addColumns)) {
            foreach ($this->addColumns as $k => $addColumn) {
                $template = $this->withEscape($addColumn['template']($columnsIds));
                $columnDefs[] = [
                    'title' => $addColumn['field'],
                    'width' => 100,
                    'template' => $template,
                ];
            }
        }
        if (count($this->editColumns) || (count($this->hyperLinks)) || count($this->headerAttributes) || count($this->hidden)) {
            foreach ($columnDefs as $k0 => $column) {
                $colName = (isset($column['field']) ? $column['field'] : $column['title']);
                foreach ($this->headerAttributes as $k => $headerAttribute) {
                    if ($headerAttribute['field'] === $colName) {
                        $columnDefs[$k0]['headerAttributes'] = $headerAttribute;
                    }
                }
                foreach ($this->hyperLinks as $k => $hyperLink) {
                    if ($hyperLink['field'] === $colName) {
                        $columnDefs[$k0]['template'] = '<a href="' . $hyperLink['url'] . '?' . $hyperLink['input_name'] . '=#: ' . $hyperLink['target_id'] . ' #">#: ' . $colName . ' # </a>';
                    }
                }
                foreach ($this->editColumns as $k1 => $editColumn) {
                    if ($editColumn['field'] === $colName) {
                        $columnDefs[$k0]['template'] = $this->withEscape($editColumn['template']($columnsIds));
                    }
                }
                foreach ($this->hidden as $k => $hidden) {
                    if ($hidden === $colName) {
                        $columnDefs[$k0]['hidden'] = true;
                    }
                }
            }
        }
        $action = [];
        if (count($this->actions)) {
            $template = "";
            foreach ($this->actions as $k => $action) {
                if ($action['condition']) {
                    $template .= $action['condition']($columnsIds, $action['template']);
                } else {
                    $template .= $action['template'];
                }
            }
            $action = [
                'title' => 'Action',
                'width' => 100,
                'template' => $template,
            ];
        }
        if ($this->action) {
            $action = [
                'title' => 'Action',
                'width' => 100,
                'template' => $this->action,
            ];
        }
        if (count($action)) {
            $columnDefs[] = $action;
        }

        $defaultColDef = [
            'sortable' => true,
            'filter' => true,
            'resizable' => true,
        ];
        $ag_grid = [
            'defaultColDef' => $defaultColDef,
            'groupDefs' => $groupDefs,
            'columnDefs' => $columnDefs,
            'schemaDefs' => $schemaDefs,
            'rowData' => $data,
            'rowAggregate' => $rowAggregate,
        ];
        return [
            'table' => $ag_grid,
        ];
    }
}
