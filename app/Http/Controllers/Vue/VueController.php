<?php

namespace App\Http\Controllers\Vue;


use App\Http\Controllers\Controller;

class VueController extends Controller
{
    public function index()
    {
        return view('layouts.vue');
    }

    public function print()
    {
        return view('layouts.print');
    }
}
