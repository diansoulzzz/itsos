<?php

namespace App\Http\Controllers\Auth;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $validate = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::where('username', $request->email)->first();

        if (!$user || !Hash::check($request->password, $user->password)) {
            return Helper::getResponse([], ['These credentials do not match our records.'], Constant::ResponseError);
        }
        $user->token = $user->createToken('my-app-token')->plainTextToken;
        return Helper::getResponse($user);
    }

    public function verify(Request $request)
    {
        return Helper::getResponse(Auth::user());
    }

    public function postDetail(Request $request)
    {
        return Helper::getResponse();
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return Helper::getResponse();
    }
}
