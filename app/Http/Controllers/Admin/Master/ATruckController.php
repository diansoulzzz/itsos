<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\Modul;
use App\Models\Truck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ATruckController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Truck::find($uid);
            return view('admin.menus.master.truck.detail', compact('detail'));
        }
        return view('admin.menus.master.truck.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select t.id, t.no_pol as Nopol, t.jenis as Jenis, t.type, t.keterangan from truck t where t.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('truck.detail'), 'id', 'la la-edit', '')
                ->addAction(route('truck.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.truck.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'no_pol' => 'required',
            'jenis' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Truck Baru Berhasil Dibuat';

            $data = new Truck();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Truck::find($uid);
                $message = 'Truck Berhasil Diedit';
            }
            $data->no_pol = $inputs->input('no_pol');
            $data->jenis = $inputs->input('jenis');
            $data->type = $inputs->input('type');
            $data->save();
            return Helper::redirect('truck.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Truck::find($uid)->delete();

        return Helper::redirect('truck.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
