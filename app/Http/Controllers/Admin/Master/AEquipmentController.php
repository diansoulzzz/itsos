<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Equipment;
use App\Models\EquipmentDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Validator;

class AEquipmentController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
//        return $uid;
        $equipment = Equipment::where('id', $uid)->get();
        $data = EquipmentDetail::with(['users'])->where('equipment_header_id', $uid)->orderBy('tanggal', 'desc')->get();

        return view('admin.menus.master.equipment.detail', compact('equipment', 'data'));
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        if ($request->ajax()) {
            $query = "select e.id, e.model_series, cast(e.tanggal as DATE) as Tanggal, e.perawatan, e.status from equipment e
                        join company c on e.company_id = c.id
                        where e.deleted_at is null and e.company_id = '" . $company . "'";
            $grid = new Grid($request, $query);
            $grid->editColumn('status', function ($data) {
                $gridHelper = new GridHelper($data);
                return $gridHelper
                    ->if($data->status, '==', 'STANDBY', function ($data) {
                        return '<a href="' . route('equipment_expenses.status', ['id' => $data->id]) . '" class="btn btn-sm btn-success" data-sw-title="Ubah Status Menjadi DISEWA?">STANDBY
                        </a>';
                    })
                    ->if($data->status, '==', 'DISEWA', function ($data) {
                        return '<a href="' . route('equipment_expenses.status', ['id' => $data->id]) . '" class="btn btn-sm btn-warning" data-sw-title="Ubah Status Menjadi MAINTENANCE?">DISEWA
                        </a>';
                    })
                    ->if($data->status, '==', 'MAINTENANCE', function ($data) {
                        return '<a href="' . route('equipment_expenses.status', ['id' => $data->id]) . '" class="btn btn-sm btn-danger" data-sw-title="Ubah Status Menjadi STANDBY?">MAINTENANCE
                        </a>';
                    })->getResult();
            })
                ->addHyperLinkOn('model_series', route('equipment_expenses.detail'), 'id', 'id')
                ->addAction(route('equipment_expenses.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog');
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.equipment.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postModel(Request $request)
    {
        $inputs = Helper::merge($request);
        try {
            $message = 'Model Serie Berhasil Ditambahkan';
            $data = new Equipment();
            $data->model_series = $inputs->input('model_series');
            $data->company_id = Auth::user()->active_company->id;
            $data->status = 'STANDBY';
            $data->save();

            return Helper::redirect('equipment_expenses.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function postDetail(Request $request)
    {
        $detail = new EquipmentDetail();
        $header_id = $request->input('header_id');
        $detail->equipment_header_id = $header_id;
        $detail->tanggal = $request->input('tanggal');
        $detail->jenis_biaya = $request->input('jenis_biaya');
        $detail->jumlah = $request->input('jumlah');
        $detail->supplier = $request->input('supplier');
        $detail->keterangan = $request->input('keterangan');
        $detail->users_id = auth()->id();
        $detail->save();

        $query2 = "select ed.jenis_biaya as status, ed.tanggal as tanggal from equipment e
                    join equipment_detail ed on e.id = ed.equipment_header_id
                    join users u on ed.users_id = u.id
                    where e.id = $header_id
                    order by ed.id desc limit 1;";
        $data = Helper::selectMany($query2);
        $status = $data[0]->status;
        $tanggal = $data[0]->tanggal;

        DB::table('equipment')
            ->join('equipment_detail', 'equipment_detail.equipment_header_id', '=', 'equipment.id')
            ->where('equipment.id', $header_id)
            ->update(['equipment.perawatan' => $status, 'equipment.tanggal' => $tanggal]);

        $message = "Detail Perawatan Tersimpan";

        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function setStatus(Request $request)
    {
        $uid = $request->input('id');
        $status = $request->input('status');
        $data = Equipment::find($uid);
//        return $data;
        $message = '';
        if ($data->status == 'STANDBY') {
            $message = 'DISEWA';
        } else if ($data->status == 'DISEWA') {
            $message = 'MAINTENANCE';
        } else if ($data->status == 'MAINTENANCE') {
            $message = 'STANDBY';
        } else {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, 'invalid status');
        }
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
//        return $message;
        $data->status = $message;
        $data->save();
        $message = $data->model_series . " STATUS: " . $message;
        return Helper::redirect('equipment_expenses.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Equipment::find($uid)->delete();

        return Helper::redirect('equipment_expenses.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function getQuery()
    {
        $company = Auth::user()->active_company->id;
        $query = "select e.id, e.model_series, e.tanggal, e.perawatan, e.status from equipment e
                    where e.company_id = '" . $company . "'";

        return DB::select(DB::raw($query));
    }

    public function deleteDataDetail(Request $request)
    {
        if ($request->has('id')) {
            $detail = EquipmentDetail::find($request->get('id'));
            $header_id = $detail->equipment_header_id;
            $detail->delete();
            $query2 = "select ed.jenis_biaya as status, ed.tanggal as tanggal from equipment e
                    join equipment_detail ed on e.id = ed.equipment_header_id
                    join users u on ed.users_id = u.id
                    where e.id = $header_id
                    order by ed.id desc limit 1;";
            $data = Helper::selectMany($query2);
            if($data != null){
                $status = $data[0]->status;
                $tanggal = $data[0]->tanggal;
                DB::table('equipment')
                    ->join('equipment_detail', 'equipment_detail.equipment_header_id', '=', 'equipment.id')
                    ->where('equipment.id', $header_id)
                    ->update(['equipment.perawatan' => $status, 'equipment.tanggal' => $tanggal]);
            }
            $message = 'DATA BERHASIL DI DELETE';
            return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        }
    }
}
