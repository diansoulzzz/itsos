<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BarangKategori;
use App\Models\DataMuatan;
use App\Models\Modul;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ABarangKategoriController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = BarangKategori::find($uid);
            return view('admin.menus.master.barang_kategori.detail', compact('detail'));
        }
        return view('admin.menus.master.barang_kategori.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select bk.id, bk.nama from barang_kategori bk where bk.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('barang_kategori.detail'), 'id', 'la la-edit', '')
                ->addAction(route('barang_kategori.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.barang_kategori.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Kategori Barang Berhasil Dibuat';

            $data = new BarangKategori();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = BarangKategori::find($uid);
                $message = 'Data Kategori Barang Berhasil Diedit';
            }
            $data->nama = $inputs->input('nama');
            $data->save();

            DB::commit();
            return Helper::redirect('barang_kategori.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {

            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        BarangKategori::find($uid)->delete();

        return Helper::redirect('barang_kategori.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
