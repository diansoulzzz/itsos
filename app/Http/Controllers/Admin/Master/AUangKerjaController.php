<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\Equipment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;

class AUangKerjaController extends Controller
{
    public function indexDetail(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $equipment = Equipment::where('company_id', $company)->get();
        return view('admin.menus.master.uang_kerja.detail', compact('equipment'));
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $kode_data = 'UANG_KERJA';
        if ($request->ajax()) {
            $query = "select dm.id, dm.no_trip as no_uang_kerja, e.model_series, dm.penerima, CAST(dm.tanggal as Date) as Tanggal, dm.penyewa, dm.jumlah as Nilai_Sangu, u.nama as Pengirim from data_muatan dm
                        join equipment e on dm.equipment_id = e.id
                        join users u on dm.users_id = u.id
                        join company c on dm.company_id = c.id
                        where dm.deleted_at is null and dm.kode_data = '" . $kode_data . "' and dm.company_id = '" . $company . "'
                        order by dm.id desc";
            $grid = new Grid($request, $query);
            if (Helper::checkAccess(request(), 'DELETE')) {
                $grid->addAction(route('uang_kerja.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog');
            }
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.uang_kerja.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'penyewa' => 'required',
            'tanggal' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        $company = Auth::user()->active_company->id;
        DB::beginTransaction();
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'UK%')
                ->where('company_id', $company)
                ->latest()
                ->first();

//            return $q;
            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'UK1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 2, 5)) + 1;
                $new_code = 'UK' . $last;
            }

//            return $last;
            $uid = $inputs->input('id');
            $message = 'Data Uang Kerja Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Uang Kerja Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'UANG_KERJA';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->jumlah = $inputs->input('jumlah');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = auth()->id();
            $data->equipment_id = $inputs->input('equipment_id');
            $data->company_id = $company;
            $data->save();

            DB::commit();
            return Helper::redirect('uang_kerja.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('uang_kerja.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
