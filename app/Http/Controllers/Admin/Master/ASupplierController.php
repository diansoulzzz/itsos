<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\Modul;
use App\Models\Supplier;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ASupplierController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Supplier::find($uid);
            return view('admin.menus.master.supplier.detail', compact('detail'));
        }
        return view('admin.menus.master.supplier.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select s.id, s.kode, s.nama from supplier s where s.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('supplier.detail'), 'id', 'la la-edit', '')
                ->addAction(route('supplier.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.supplier.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Supplier Baru Berhasil Dibuat';

            $data = new Supplier();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Supplier::find($uid);
                $message = 'Data Supplier Berhasil Diedit';
            }
            $data->kode = $inputs->input('kode');
            $data->nama = $inputs->input('nama');
            $data->save();

            DB::commit();
            return Helper::redirect('supplier.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Supplier::find($uid)->delete();

        return Helper::redirect('supplier.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
