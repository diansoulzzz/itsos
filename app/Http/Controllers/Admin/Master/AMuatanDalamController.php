<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\Truck;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMuatanDalamController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $nopol = Truck::OrderBy('jenis','desc')->get();
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.muatan_dalam.detail', compact('detail'));
        }
        return view('admin.menus.master.muatan_dalam.detail',compact('nopol'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $company = Auth::user()->active_company->id;
            $kode_data = 'MUATAN_DALAM';
            $query = "select dm.id, dm.no_trip as No_Trip, dm.penerima, CAST(dm.tanggal as Date) as Tanggal, dm.penyewa, dm.muatan, dm.no_pol as No_Pol, dm.jumlah as Nilai_Sangu, dm.asal as Awal, dm.tujuan, dm.nilai_awal as Total_Tagihan, u.nama as Pengirim from data_muatan dm
                        join users u on dm.users_id = u.id
                        join company c on dm.company_id = c.id
                        where dm.deleted_at is null and dm.kode_data = '" . $kode_data . "' and dm.company_id = '" . $company . "'";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('muatan_dalam.detail'), 'id', 'la la-edit', '')
                ->addAction(route('muatan_dalam.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.muatan_dalam.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'D%')
                ->latest()
                ->first();

            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'D1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 1, 5)) + 1;
                $new_code = 'D' . $last;
            }
            $uid = $inputs->input('id');
            $message = 'Data Muatan Dalam Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Muatan Dalam Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_DALAM';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->truck_id = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->asal = $inputs->input('asal');
            $data->tujuan = $inputs->input('tujuan');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->users_id = auth()->id();
            $data->company_id = Auth::user()->active_company->id;
            $data->save();
            return Helper::redirect('muatan_dalam.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('muatan_dalam.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function approve(Request $request)
    {
        $message = 'Approved';
        $uid = $request->input('id');
//        return $uid;
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DB::table('data_muatan')
            ->where('id', $uid)
            ->update(['approve' => 1]);

        return Helper::redirect('muatan_dalam.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
//    public function getQuery(Request $request)
//    {
//        $timezone = 'ASIA/JAKARTA';
//        $today = Carbon::today()->setTimezone($timezone)->toDateString();
//
//        $query ='select d.id, d.no_trip, d.penerima, d.jumlah, d.jenis_biaya, t.no_pol, d.penyewa, d.tanggal, d.muatan, d.asal, d.tujuan, d.keterangan, u.username, d.approve d.deleted_at from data_muatan d
//                    join truck t on d.truck_id = t.id
//                    join users u on d.users_id = u.id
//                    where kode_data like \'MUATAN_DALAM\' and d.deleted_at is null;';
//
//        return DB::select(DB::raw($query));
//    }
    public function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = DataMuatan::with(['users','truck'])->where('kode_data', 'MUATAN_DALAM')->where('tanggal',$today);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                if (Helper::checkAccess($request, 'DELETE')) {
                    $actions .= '<a href="javascript:void(0);" data-url="' . url('master/muatan_dalam/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                }
                return $actions;
            })
            ->editColumn('approve', function ($data) use ($request) {
                $approve = "";
                if (Helper::checkAccess($request, 'APPROVE')) {
                    if ($data->approve == 0) {
                        $approve .= '<a href="' . url('master/muatan_dalam/app?id=' . $data->id) . '" class="btn btn-sm btn-danger">NO
                    </a>';
                    } else {
                        $approve .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">APPROVED</span>';
                    }
                }
                return $approve;
            })
            ->editColumn('tanggal', function ($data) {
                return date('Y-m-d', strtotime($data->tanggal));
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
