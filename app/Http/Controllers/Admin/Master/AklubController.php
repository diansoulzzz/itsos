<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AKlubController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $users = User::get();
        $pengkot = Pengkot::get();
        if ($uid) {
            $detail = Klub::with(['users', 'pengkot'])->find($uid);
            return view('admin.menus.master.klub.detail', compact('detail'));
        }
        return view('admin.menus.master.klub.detail',compact('users','pengkot'));
    }
    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select k.id as id, k.nama_klub as tim, k.alamat as alamat, k.telpon as telpon, k.keterangan as keterangan, u.nama as Manager, p.nama as pengkot from klub k
                        join users u on k.users_id = u.id
                        join pengkot p on k.pengkot_id = p.id
                        where k.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('klub.detail'), 'id', 'la la-edit', '')
                ->addAction(route('klub.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.klub.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama_klub' => 'required',
            'telpon' => 'required',
            'alamat' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Tim Baru Berhasil Dibuat';

            $data = new Klub();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Klub::find($uid);
                $message = 'Tim Berhasil Diedit';
            }
            $data->nama_klub = $inputs->input('nama_klub');
            $data->alamat = $inputs->input('alamat');
            $data->telpon = $inputs->input('telpon');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = $inputs->input('manager');
            $data->pengkot_id = $inputs->input('pengkot');
            $data->save();
            return Helper::redirect('klub.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

//        $invalid = $this->isInvalid($uid);
//        if ($invalid) {
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
//        }
//        return $uid;

        Klub::find($uid)->delete();

        return Helper::redirect('klub.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function dataTable(Request $request)
    {
        $timezone = 'ASIA/JAKARTA';
        $today = Carbon::today()->setTimezone($timezone)->toDateString();
        $model = Klub::query()->with(['users', 'pengkot']);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $actions = "";
                $actions .= '<a href="javascript:void(0);" data-url="' . url('master/muatan_luar/del?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
                        <i class="la la-trash"></i>
                    </a>';
                return $actions;
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }
}
