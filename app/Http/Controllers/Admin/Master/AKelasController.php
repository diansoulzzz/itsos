<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Kela;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;

class AKelasController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Kela::find($uid);
            return view('admin.menus.master.kelas.detail', compact('detail'));
        }
        return view('admin.menus.master.kelas.detail');
    }
    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select k.id, k.kategori_kelas, k.kelas from kelas k
                        where k.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('kelas.detail'), 'id', 'la la-edit', '')
                ->addAction(route('kelas.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.kelas.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kategori_kelas' => 'required',
            'kelas' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Kelas Baru Berhasil Dibuat';

            $data = new Kela();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Kela::find($uid);
                $message = 'Data Muatan Luar Berhasil Diedit';
            }
            $data->kategori_kelas = $inputs->input('kategori_kelas');
            $data->kelas = $inputs->input('kelas');
            $data->save();
            return Helper::redirect('kelas.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

//        $invalid = $this->isInvalid($uid);
//        if ($invalid) {
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
//        }
//        return $uid;

        Kela::find($uid)->delete();

        return Helper::redirect('kelas.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
