<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BarangKategori;
use App\Models\DataMuatan;
use App\Models\Mekanik;
use App\Models\Modul;
use App\Models\Pelanggan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class APelangganController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Pelanggan::find($uid);
            return view('admin.menus.master.pelanggan.detail', compact('detail'));
        }
        return view('admin.menus.master.pelanggan.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select p.id, p.kode, p.nama from pelanggan p where p.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('pelanggan.detail'), 'id', 'la la-edit', '')
                ->addAction(route('pelanggan.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.pelanggan.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kode' => 'required',
            'nama_pria' => 'required',
            'nama_wanita' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Pelanggan Berhasil Disimpan';

            $data = new Pelanggan();
            $nama_pria = $inputs->input('nama_pria');
            $tgl_lahir_pria = $request->input('tanggal_lahir_pria');
            $nama_wanita = $inputs->input('nama_wanita');
            $tgl_lahir_wanita = $request->input('tanggal_lahir_wanita');
            $nama = $nama_pria.' & '.$nama_wanita;

            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Pelanggan::find($uid);
                $message = 'Data Pelanggan Berhasil Diedit';
            }
            $data->kode = $inputs->input('kode');
            $data->nama = $nama;
            $data->nama_1 = $nama_pria;
            $data->nama_2 = $nama_wanita;
            $data->tgl_lahir_1 = $tgl_lahir_pria;
            $data->tgl_lahir_2 = $tgl_lahir_wanita;

            $data->save();

            DB::commit();
            return Helper::redirect('pelanggan.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Pelanggan::find($uid)->delete();

        return Helper::redirect('pelanggan.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
