<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Pengkot;
use App\Models\Sabuk;
use App\Models\Truck;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class ASabukController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Sabuk::find($uid);
            return view('admin.menus.master.sabuk.detail', compact('detail'));
        }
        return view('admin.menus.master.sabuk.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select s.id, s.geup, s.keterangan from sabuk s
                        where s.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('sabuk.detail'), 'id', 'la la-edit', '')
                ->addAction(route('sabuk.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.sabuk.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'geup' => 'required',
            'keterangan' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Geup Baru Berhasil Dibuat';

            $data = new Sabuk();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Sabuk::find($uid);
                $message = 'Geup Berhasil Diedit';
            }
            $data->geup = $inputs->input('geup');
            $data->keterangan = $inputs->input('keterangan');
            $data->save();
            return Helper::redirect('sabuk.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        Sabuk::find($uid)->delete();

        return Helper::redirect('sabuk.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
