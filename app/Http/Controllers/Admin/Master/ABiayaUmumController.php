<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ABiayaUmumController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.biaya_umum.detail', compact('detail'));
        }
        return view('admin.menus.master.biaya_umum.detail');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $kode_data = 'BIAYA_UMUM';
        if ($request->ajax()) {
            $query = "select dm.id, dm.no_trip as No_Trip, dm.penerima, CAST(dm.tanggal as Date) as Tanggal, dm.penyewa, dm.muatan, dm.no_pol as No_Pol, dm.jumlah as Nilai_Sangu, dm.asal as Awal, dm.tujuan, dm.nilai_awal as Total_Tagihan, u.nama as Pengirim from data_muatan dm
                        join users u on dm.users_id = u.id
                        join company c on dm.company_id = c.id
                        where dm.deleted_at is null and dm.kode_data = '" . $kode_data . "' and dm.company_id = '" . $company . "'";;
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('biaya_umum.detail'), 'id', 'la la-edit', '')
                ->addAction(route('biaya_umum.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.biaya_umum.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $uid = $inputs->input('id');
            $message = 'Biaya Umum Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Biaya Umum Berhasil Diedit';
            }
            $data->kode_data = 'BIAYA_UMUM';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = auth()->id();
            $data->company_id = Auth::user()->active_company->id;
            $data->save();
            return Helper::redirect('biaya_umum.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('biaya_umum.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
