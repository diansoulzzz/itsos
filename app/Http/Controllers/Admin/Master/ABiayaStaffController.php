<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;
use App\Models\DataMuatan;

class ABiayaStaffController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.biaya_staff.detail', compact('detail'));
        }
        return view('admin.menus.master.biaya_staff.detail');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $kode_data = 'BIAYA_STAFF';
        if ($request->ajax()) {
            $query = "select dm.id, dm.penerima, CAST(dm.tanggal as Date) as Tanggal, dm.jumlah as Nilai_Sangu, u.nama as Pengirim from data_muatan dm
                        join users u on dm.users_id = u.id
                        join company c on dm.company_id = c.id
                        where dm.deleted_at is null and dm.kode_data = '" . $kode_data . "' and dm.company_id = '" . $company . "'
                        order by dm.id desc";
            $grid = new Grid($request, $query);
            $result = $grid
//                ->addAction(route('biaya_staff.detail'), 'id', 'la la-edit', '')
                ->addAction(route('biaya_staff.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.biaya_staff.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('biaya_staff.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Biaya Staff Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Biaya Staff Berhasil Diedit';
            }
            $data->kode_data = 'BIAYA_STAFF';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->jumlah = $inputs->input('jumlah');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = auth()->id();
            $data->company_id = Auth::user()->active_company->id;
            $data->save();
            return Helper::redirect('biaya_staff.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }
}
