<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Kategori;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AKategoriController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Kategori::find($uid);
            return view('admin.menus.master.kategori.detail', compact('detail'));
        }
        return view('admin.menus.master.kategori.detail');
    }
    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select k.id, k.kategori from kategori k
                        where k.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('kategori.detail'), 'id', 'la la-edit', '')
                ->addAction(route('kategori.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.kategori.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kategori' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        try {
            $uid = $inputs->input('id');
            $message = 'Kategori Baru Berhasil Dibuat';

            $data = new Kategori();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Kategori::find($uid);
                $message = 'Data Kategori Berhasil Diedit';
            }
            $data->kategori = $inputs->input('kategori');
            $data->save();
            return Helper::redirect('kategori.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

//        $invalid = $this->isInvalid($uid);
//        if ($invalid) {
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
//        }
//        return $uid;

        Kategori::find($uid)->delete();

        return Helper::redirect('kategori.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
