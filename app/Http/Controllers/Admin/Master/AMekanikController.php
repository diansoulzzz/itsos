<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BarangKategori;
use App\Models\DataMuatan;
use App\Models\Mekanik;
use App\Models\Modul;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class AMekanikController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Mekanik::find($uid);
            if ($detail) {
                return Helper::getResponse($detail);
            }
            return Helper::getResponse([], ['not found'], Constant::ResponseError);
//            return view('admin.menus.master.mekanik.detail', compact('detail'));
        }
        return Helper::getResponse([]);
//        return view('admin.menus.master.mekanik.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax() || $request->acceptsJson()) {
            $query = 'select k.id, k.kode, k.nama from mekanik k where k.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('mekanik.detail'), 'id', 'la la-edit', '')
                ->addAction(route('mekanik.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.mekanik.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
//            'tes' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Mekanik Berhasil Ditambah';

            $data = new Mekanik();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Mekanik::find($uid);
                $message = 'Data Mekanik Berhasil Diedit';
            }
            $data->kode = $inputs->input('kode');
            $data->nama = $inputs->input('nama');
            $data->save();

            DB::commit();
//            return Helper::redirect('mekanik.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
            return Helper::getResponse($data);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::getResponse([], $e->getMessage(), Constant::ResponseError);
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Mekanik::find($uid)->delete();

        return Helper::redirect('mekanik.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
