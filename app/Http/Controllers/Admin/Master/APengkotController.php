<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Pengkot;
use App\Models\Truck;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class APengkotController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Pengkot::with(['users'])->find($uid);
            return view('admin.menus.master.pengkot.detail', compact('detail'));
        }
        $users = User::get();
        return view('admin.menus.master.pengkot.detail',compact('users'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select p.id, p.nama as Pengkot, u.nama as Ketua, p.alamat, p.telpon as Telephone, p.keterangan from pengkot p
                        join users u on p.users_id = u.id
                        where p.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('pengkot.detail'), 'id', 'la la-edit', '')
                ->addAction(route('pengkot.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.pengkot.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required',
            'ketua' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Pengkot Baru Berhasil Dibuat';

            $data = new Pengkot();
            if($uid){
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Pengkot::find($uid);
                $message = 'Pengkot Berhasil Diedit';
            }
            $data->nama = $inputs->input('nama');
            $data->alamat = $inputs->input('alamat');
            $data->telpon = $inputs->input('telpon');
            $data->keterangan = $inputs->input('keterangan');
            $data->users_id = $inputs->input('ketua');
            $data->save();

            DB::commit();
            return Helper::redirect('pengkot.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        Pengkot::find($uid)->delete();

        return Helper::redirect('pengkot.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
