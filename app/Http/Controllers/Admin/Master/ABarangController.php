<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\DataMuatan;
use App\Models\Modul;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ABarangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Barang::with(['barang_kategori','barang_satuans'])->find($uid);
//            return $detail;
            return view('admin.menus.master.barang.detail', compact('detail'));
        }
        return view('admin.menus.master.barang.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'select b.id, b.kode, b.nama, bk.nama as Kategori from barang b
                        join barang_kategori bk on b.barang_kategori_id = bk.id
                        where b.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('barang.detail'), 'id', 'la la-edit', '')
                ->addAction(route('barang.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.barang.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Kategori Barang Berhasil Dibuat';

            $data = new Barang();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Barang::with(['barang_kategori','barang_satuans'])->find($uid);
                $message = 'Data Kategori Barang Berhasil Diedit';
            }
            $data->kode = $inputs->input('kode');
            $data->nama = $inputs->input('nama');
            $data->barang_kategori_id = $inputs->input('barang_kategori')->id;
            $data->save();

            $barang_satuan_ds = [];

            $detail = $inputs->input('barang_satuans');
            foreach ($detail as $key => $value) {
                if ($value['kode'] <> '') {
                    $barang_satuan = [
                        'id' => $value['id'],
                        'barang_id' => $data['id'],
                        'kode' => $value['kode'],
                        'rasio' => $value['rasio'],
                        'keterangan' => $value['keterangan'],
                    ];
                    $barang_satuan_ds[] = $barang_satuan;
                }
            }

//            return $request->all();

//            if (!collect($barang_satuan_ds)->contains('rasio', 1)) {
//                return 'abc';
//                $message2 = 'Gagal menyimpan data Rasio 1 Wajib ada!';
//                return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message2);
//            }
            Helper::syncMany($data->barang_satuans(), $barang_satuan_ds, 'id');

            DB::commit();
            return Helper::redirect('barang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {

            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Barang::find($uid)->delete();

        return Helper::redirect('barang.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
