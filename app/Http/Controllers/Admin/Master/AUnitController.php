<?php

namespace App\Http\Controllers\Admin\Master;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Mekanik;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AUnitController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Unit::find($uid);
            return view('admin.menus.master.unit.detail', compact('detail'));
        }
        return view('admin.menus.master.unit.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select u.id, u.kode, u.nama from unit u where u.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('unit.detail'), 'id', 'la la-edit', '')
                ->addAction(route('unit.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.master.unit.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'kode' => 'required',
            'nama' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Unit Berhasil Ditambah';

            $data = new Unit();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Unit::find($uid);
                $message = 'Data Unit Berhasil Diedit';
            }
            $data->kode = $inputs->input('kode');
            $data->nama = $inputs->input('nama');
            $data->save();

            DB::commit();
            return Helper::redirect('unit.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Unit::find($uid)->delete();

        return Helper::redirect('unit.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
