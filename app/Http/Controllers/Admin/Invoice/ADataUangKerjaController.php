<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ADataUangKerjaController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.data_trip.detail', compact('detail'));
        }
        return view('admin.menus.invoice.data_trip.detail');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $kode_data = 'UANG_KERJA';
        if ($request->ajax()) {
            $query = "select dm.id, dm.no_trip, dm.penyewa, cast(dm.tanggal as DATE) AS Tanggal, dm.penerima, dm.jumlah as Nilai_Sangu, e.model_series, dm.keterangan, u.username as Pengirim from data_muatan dm
                        join equipment e on dm.equipment_id = e.id
                        join users u on dm.users_id = u.id
                        where dm.company_id = '" . $company . "' AND dm.kode_data = '" . $kode_data . "' AND dm.flag = 0
                        order by dm.tanggal desc" ;
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('data_uang_kerja.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->withCheckBox(true)
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.invoice.data_uang_kerja.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('data_uang_kerja.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
