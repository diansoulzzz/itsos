<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\DataInvoiceHeader;
use App\Models\DataInvoicePrint;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class AApproveController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataInvoicePrint::with(['users'])->find($uid);
            return view('admin.menus.master.data_trip.detail', compact('detail'));
        }
        return view('admin.menus.invoice.list_invoice.detail');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        if ($request->ajax()) {
            $query = "select dih.id, dih.no_invoice,dih.no_trip, dip.approve, dih.keterangan, sum(dm.jumlah) as jumlah, (dih.total_tagihan+dih.ppn) as tagihan, u.username as pengirim from users u
                    join data_invoice_print dip on u.id = dip.users_id
                    join data_invoice_header dih on dip.data_invoice_header_id = dih.id
                    join data_invoice_detail did on dih.id = did.data_invoice_header_id
                    join data_muatan dm on did.data_muatan_id = dm.id
                    where dip.company_id = '" . $company . "'
                    group by dih.id, dih.no_invoice, dih.no_trip, dih.kepada, dip.approve, dih.keterangan, u.username, tagihan
                    order by dih.id desc";
            $grid = new Grid($request, $query);
            if (Helper::checkAccess(request(), 'APPROVE')) {
                $grid->editColumn('approve', function ($data) {
                    $gridHelper = new GridHelper($data);
                    return $gridHelper
                        ->if($data->approve, '==', 0, function ($data) {
                            return '<a href="' . route('approve.app', ['id' => $data->id]) . '" class="btn btn-sm btn-danger">NO
                        </a>';
                        })
                    ->if($data->approve, '!=', 0, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">APPROVED</span>';
                    })->getResult();
                });
            }
            if (Helper::checkAccess(request(), 'PRINT')) {
                $grid->addAction(route('olap.print_out.view', ['print' => 'Invoice']), 'id', 'la la-print', '');
            }
            $grid->addHyperLinkOn('no_invoice', route('approve.deskripsi'), 'id', 'id')
                ->setHidden(['tagihan']);
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.invoice.approve.list');
    }

    public
    function isInvalid($uid)
    {
        return false;
    }

    public
    function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataInvoicePrint::find($uid)->delete();

        return Helper::redirect('approve.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function approve(Request $request)
    {
        $message = 'Invoice Berhasil di Approve';
        $uid = $request->input('id');
//        return $uid;
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DB::table('data_invoice_print')
            ->where('data_invoice_header_id', $uid)
            ->update(['approve' => 1]);

        return Helper::redirect('approve.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function deskripsi(Request $request)
    {
        $uid = $request->input('id');
        $data = DB::table('data_invoice_print')
            ->join('data_invoice_header', 'data_invoice_print.data_invoice_header_id', '=', 'data_invoice_header.id')
            ->select('data_invoice_print.id', 'data_invoice_header.no_trip', 'data_invoice_header.tanggal', 'data_invoice_header.total_tagihan', 'data_invoice_print.deskripsi')
            ->where('data_invoice_print.data_invoice_header_id', '=', $uid)
            ->get();

//        return $data;

        return view('admin.menus.invoice.approve.deskripsi', compact('data'));
    }

    public
    function post_deskripsi(Request $request)
    {
        $invoice = $request->input('invoice');
        foreach ($invoice as $i => $val) {
//            echo $i . "=" . $val['description'];
            DB::table('data_invoice_print')
                ->where('id', $i)
                ->update(['deskripsi' => $val['description']]);
        }

        $message = "SUKSES";
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function getQuery(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $query = "select dih.id, dih.no_invoice,dih.no_trip, dip.approve, dih.keterangan, sum(dm.jumlah) as jumlah, (dih.total_tagihan+dih.ppn) as tagihan, u.username from users u
                    join data_invoice_print dip on u.id = dip.users_id
                    join data_invoice_header dih on dip.data_invoice_header_id = dih.id
                    join data_invoice_detail did on dih.id = did.data_invoice_header_id
                    join data_muatan dm on did.data_muatan_id = dm.id
                    where dip.company_id = '" . $company . "'
                    group by dih.id, dih.no_invoice, dih.no_trip, dih.kepada, dip.approve, dih.keterangan, u.username, tagihan
                    order by dih.id desc";

        return DB::select(DB::raw($query));
    }

    public
    function dataTable(Request $request)
    {
        $model = $this->getQuery($request);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) use ($request) {
                $action = "";
                if (Helper::checkAccess($request, 'PRINT')) {
                    if ($data->approve == 1) {
                        $action .= '<a href="' . route('olap.print_out.view', ['print' => Constant::PrintInvoice, 'id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Print">
                        <i class="la la-print"></i>
                    </a>';
                    }
                }
                return $action;
            })
            ->editColumn('approve', function ($data) use ($request) {
                $approve = "";
                if (Helper::checkAccess($request, 'APPROVE')) {
                    if ($data->approve == 0) {
                        $approve .= '<a href="' . url('invoice/approve/app?id=' . $data->id) . '" class="btn btn-sm btn-danger">NO
                    </a>';
                    } else {
                        $approve .= '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">APPROVED</span>';
                    }
                }
                return $approve;
            })
            ->editColumn('jumlah', function ($data) {
                return (number_format($data->jumlah, '0', '.', ','));
            })
            ->editColumn('tagihan', function ($data) {
                return (number_format($data->tagihan, '0', '.', ','));
            })
            ->editColumn('no_invoice', function ($data) {
                return '<u><b><a href="' . url('invoice/approve/deskripsi?id=' . $data->id) . '">' . $data->no_invoice . '</b></u></a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

//if (data.getData("ppn")>0) {
//Text20.enabled=true;
//Text21.enabled=true;
//Text16.enabled=true;
//Text26.setEnabled(true);
//Text27.setTop(0);
//}
}
