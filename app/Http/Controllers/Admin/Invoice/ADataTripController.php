<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ADataTripController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = DataMuatan::with(['users'])->find($uid);
            return view('admin.menus.master.data_trip.detail', compact('detail'));
        }
        return view('admin.menus.invoice.data_trip.detail');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        $kode_data = 'MUATAN_LUAR';
        if ($request->ajax()) {
            $query = "select dm.id, dm.no_trip as No_Trip, dm.penerima, CAST(dm.tanggal as Date) as Tanggal, dm.penyewa, dm.muatan, dm.no_pol as No_Pol, dm.jumlah as Nilai_Sangu, dm.asal as Awal, dm.tujuan, dm.nilai_awal as Total_Tagihan, u.nama as Pengirim from data_muatan dm
                        join users u on dm.users_id = u.id
                        join company c on dm.company_id = c.id
                        where dm.deleted_at is null and dm.kode_data = '" . $kode_data . "' and dm.company_id = '" . $company . "'";;
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('data_trip.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->withCheckBox(true)
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.invoice.data_trip.list');
    }
    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        DataMuatan::find($uid)->delete();

        return Helper::redirect('data_trip.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
