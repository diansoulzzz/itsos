<?php

namespace App\Http\Controllers\Admin\Invoice;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\DataInvoiceHeader;
use App\Models\DataInvoicePrint;
use App\Models\DataMuatan;
use App\Models\Pelunasan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class ADataController extends Controller
{
    public function indexDetail(Request $request)
    {
//        $detail = DataMuatan::where('kode_data', '=', 'MUATAN_LUAR')->with(['users'])->get();
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
        ], $message);
        $check = $request->input('check_box');
//        return $request->all();
        $detail = DataMuatan::whereIn('id', $check)->get();
        $no_trip = '';
        $nilai_awal_total = 0;
        foreach ($detail as $d) {
            $id = $d->id;
            $no_trip = $no_trip . $d->no_trip . ',';
            $penyewa = $d->penyewa;
            $nilai_awal_total = $nilai_awal_total + $d->nilai_awal;
        }
        $penyewa = $penyewa;
        $no_trip = $no_trip;
        $nilai_awal_total = $nilai_awal_total;
//        return $company;
        return view('admin.menus.invoice.list_invoice.detail', compact('detail', 'penyewa', 'no_trip', 'nilai_awal_total'));
    }

    public function indexDetailKwitansi(Request $request)
    {
//        $detail = DataMuatan::where('kode_data', '=', 'MUATAN_LUAR')->with(['users'])->get();
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'check' => 'required',
        ], $message);

        $check = $request->input('check');
        $detail = DataMuatan::whereIn('id', $check)->get();
        $no_trip = '';
        $nilai_awal_total = 0;
        foreach ($detail as $d) {
            $id = $d->id;
            $no_trip = $no_trip . $d->no_trip . ',';
            $penyewa = $d->penyewa;
            $nilai_awal_total = $nilai_awal_total + $d->nilai_awal;
        }
        $penyewa = $penyewa;
        $no_trip = $no_trip;
        $nilai_awal_total = $nilai_awal_total;
        return view('admin.menus.invoice.list_kwitansi.detail', compact('detail', 'penyewa', 'no_trip', 'nilai_awal_total'));
    }

    public function indexListKwitansi(Request $request)
    {
        $company = Auth::user()->active_company->id;
        if ($request->ajax()) {
            $query = "select dih.id, dih.no_invoice, dih.no_trip, dih.total_tagihan as Tagihan, dih.penyewa, cast(dih.tanggal as Date) as Tanggal, dih.keterangan, dih.status, dih.checker, u.nama as Pengirim from data_invoice_header dih
                        join users u on dih.users_id = u.id
                        where dih.deleted_at is null and dih.company_id = '" . $company . "'";
            $actions =
                '<a role="button" href="javascript:void(0);" data-url="' . route('kwitansi_uang_kerja.delete') . '" data-field-name="id" data-title="Yakin Hapus?" data-type="dialog" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                    <i class="la la-trash"></i>
                </a>';
            $result = Helper::generateResult($request, $query, $actions);
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.invoice.list_kwitansi.list');
    }

    public function indexList(Request $request)
    {
        $company = Auth::user()->active_company->id;
        if ($request->ajax()) {
            $query = "select dih.id, dih.no_invoice, dih.no_trip, dih.total_tagihan, dih.penyewa, cast(dih.tanggal as DATE) as Tanggal, ifnull(dih.status,0) as status, dih.keterangan,  u.username as Pengirim, dih.flag from data_invoice_header dih
                        join users u on dih.users_id = u.id
                        where dih.company_id = '" . $company . "' and dih.deleted_at is null";
            $grid = new Grid($request, $query);
            if (Helper::checkAccess(request(), 'BUAT_INVOICE')) {
                $grid->addAction(route('kwitansi.add'), 'id', 'la la-plus', '', '', '', function ($data, $template) {
                    $gridHelper = new GridHelper($data);
                    return $gridHelper
                        ->if($data->flag, '==', 0, function ($data) use ($template) {
                            return $template;
                        })->getResult();
                });
            }
            $grid->editColumn('status', function ($data) {
                $gridHelper = new GridHelper($data);
                return $gridHelper
                    ->if($data->status, '==', 0, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger kt-badge--rounded">BELUM AUDIT</span>';
                    })->if($data->status, '!=', 0, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">' . $data->status . '</span>';
                    })->getResult();
            });
            if (Helper::checkAccess(request(), 'DELETE')) {
                $grid->addAction(route('kwitansi.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog');
            }
            $grid->addHyperLinkOn('no_invoice', route('kwitansi.pembayaran'), 'id', 'id')
                ->setHidden(['flag']);
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.invoice.list_invoice.list');
    }

    public
    function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'no_invoice' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            $data = new DataInvoiceHeader();
            $data->no_invoice = $request->input('no_invoice');
            $data->no_trip = $request->input('no_trip');
            $data->tanggal = $request->input('tanggal');
            $data->nilai_awal_total = $request->input('nilai_awal_total');
            $data->total_tagihan = $request->input('total_tagihan');
            $data->ppn = $request->input('ppn');
            $data->penyewa = $request->input('penyewa');
            $data->keterangan = $request->input('keterangan');
            $data->kepada = $request->input('kepada');
            $data->alamat = $request->input('alamat');
            $data->users_id = auth()->user()->id;
            $data->company_id = Auth::user()->active_company->id;
            $data->flag = 0;
            $data->save();

            $data_muatan_id = $request->input('data_muatan_id');
            foreach ($data_muatan_id as $d) {
                $detail_invoice_d = [
                    'data_muatan_id' => $d
                ];
                DB::table('data_muatan')
                    ->where('id', $d)
                    ->update(['flag' => 1]);

                $detail_invoice_ds[] = $detail_invoice_d;
            }

            $data->data_invoice_details()->createMany($detail_invoice_ds);
            $message = 'Berhasil Dibuat';

            return Helper::redirect('kwitansi.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public
    function postDetailKwitansi(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'no_invoice' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            $data = new DataInvoiceHeader();
            $data->no_invoice = $request->input('no_invoice');
            $data->no_trip = $request->input('no_trip');
            $data->tanggal = $request->input('tanggal');
            $data->nilai_awal_total = $request->input('nilai_awal_total');
            $data->total_tagihan = $request->input('total_tagihan');
            $data->penyewa = $request->input('penyewa');
            $data->keterangan = $request->input('keterangan');
            $data->users_id = auth()->user()->id;
            $data->company_id = Auth::user()->active_company->id;
            $data->flag = 0;
            $data->save();

            $data_muatan_id = $request->input('data_muatan_id');
            foreach ($data_muatan_id as $d) {
                $detail_invoice_d = [
                    'data_muatan_id' => $d
                ];
                DB::table('data_muatan')
                    ->where('id', $d)
                    ->update(['flag' => 1]);

                $detail_invoice_ds[] = $detail_invoice_d;
            }

            $data->data_invoice_details()->createMany($detail_invoice_ds);
            $message = 'Berhasil Dibuat';

            return Helper::redirect('kwitansi_uang_kerja.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public
    function isInvalid($uid)
    {
        return false;
    }

    public
    function postInvoice(Request $request)
    {
        $uid = $request->input('id');
        try {
            $data = new DataInvoicePrint();
            $data->data_invoice_header_id = $uid;
            $data->approve = 0;
            $data->users_id = auth()->id();
            $data->company_id = Auth::user()->active_company->id;
            $data->save();

            DB::table('data_invoice_header')
                ->where('id', $uid)
                ->update(['flag' => 1]);
            $message = "BERHASIL !";

            return Helper::redirect('approve.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public
    function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
//        $uid = $request->input('id');

        $data = DataInvoiceHeader::find($request->get('id'));
//        return $data;
        DB::table('data_invoice_header')
            ->join('data_invoice_detail', 'data_invoice_detail.data_invoice_header_id', '=', 'data_invoice_header.id')
            ->join('data_muatan', 'data_invoice_detail.data_muatan_id', '=', 'data_muatan.id')
            ->where('data_invoice_header.id', $data->id)
            ->update(['data_muatan.flag' => 0]);
        $data->data_invoice_details()->delete();
        $data->delete();

        DB::table('data_invoice_print')
            ->where('data_invoice_print.data_invoice_header_id', $data->id)
            ->delete();

        DB::table('pelunasan')
            ->where('pelunasan.data_invoice_header_id', $data->id)
            ->delete();


        return Helper::redirect('kwitansi.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function pembayaran(Request $request)
    {
        $uid = $request->input('id');
        $no_kwitansi = DataInvoiceHeader::where('id', $uid)->get();
        $data = Pelunasan::with(['users'])->where('data_invoice_header_id', $uid)->get();

        return view('admin.menus.invoice.list_invoice.pembayaran', compact('no_kwitansi', 'data'));
    }

    public
    function postPembayaran(Request $request)
    {
        $pelunasan = new Pelunasan();
        $pelunasan->data_invoice_header_id = $request->input('header_id');
        $pelunasan->no_kwitansi = $request->input('no_kwitansi');
        $tanggal = $request->input('tanggal_bayar');
        $nominal = $request->input('nominal_dibayar');
        $bank = $request->input('bank');
        $status = $request->input('status');

        $pelunasan->tanggal_bayar = $tanggal;
        $pelunasan->nominal_dibayar = $nominal;
        $pelunasan->bank = $bank;
        $pelunasan->status = $status;
        $pelunasan->jumlah_tagihan = $request->input('jumlah_tagihan');
        $pelunasan->penyewa = $request->input('penyewa');
        $pelunasan->audit = 0;
        $pelunasan->users_id = auth()->id();
//        return $pelunasan;
        $pelunasan->save();
        $message = "_______PEMBAYARAN BERHASIL !";

        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public
    function audit(Request $request)
    {
        $uid = $request->input('id');
        $pelunasan = Pelunasan::find($uid);
        $status = $pelunasan->status;
        $header_id = $pelunasan->data_invoice_header_id;
        $user = auth()->user()->username;
//        return $header_id;

        $data = Pelunasan::all();
        DB::table('pelunasan')
            ->where('id', $uid)
            ->update(['audit' => 1]);

        DB::table('data_invoice_header')
            ->join('pelunasan', 'pelunasan.data_invoice_header_id', '=', 'data_invoice_header.id')
            ->where('pelunasan.id', $uid)
            ->update(['checker' => $user]);

        DB::table('data_invoice_header')
            ->join('pelunasan', 'pelunasan.data_invoice_header_id', '=', 'data_invoice_header.id')
            ->where('data_invoice_header.id', $header_id)
            ->update(['data_invoice_header.status' => $status]);

        $message = 'AUDIT SUKSES';
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
    public function deleteDataDetail(Request $request)
    {
        if ($request->has('id')) {
            $pelunasan = Pelunasan::find($request->get('id'));
            $pelunasan->delete();
//            $query2 = "select p.status from pelunasan p
//                    join data_invoice_header kh on p.data_invoice_header_id = kh.id
//                    join users u on p.users_id = u.id
//                    where kh.id = $pelunasan->data_invoice_header_id
//                    order by p.tanggal_bayar desc limit 1";
//            $data = Helper::selectMany($query2);
//            $status = $data[0]->status;
//            return $status;

            $message = 'DATA BERHASIL DI DELETE';
            return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        }
    }
}
