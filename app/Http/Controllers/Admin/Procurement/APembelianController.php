<?php

namespace App\Http\Controllers\Admin\Procurement;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\OrderKerja;
use App\Models\Pembelian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class APembelianController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Pembelian::with('pembelian_details.barang', 'supplier')->find($uid);
            return view('admin.menus.procurement.pembelian.detail', compact('detail'));
        }
        return view('admin.menus.procurement.pembelian.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'select p.id, p.kode_nota, CAST(p.tgl as DATE) as Tanggal, s.nama as Supplier, p.grand_total as GrandTotal, u.nama as CreatedBy from pembelian p
                        join supplier s on p.supplier_id = s.id
                        join users u on p.created_by = u.id
                        where p.deleted_at is null;';
            $grid = new Grid($request, $query);
            $result = $grid
//                ->addHyperLinkOn('Supplier', '', 'nama_supplier', 'Supplier')
//                ->editColumn('kode_nota', function ($data) {
//                    return '<a href="' . url('master/biaya_kir?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
//                        <i class="la la-trash"></i>
//                    </a>';
//                })
//                ->addColumn('kontol', function ($data) {
//                    return '<a href="' . url('master/biaya_kir?id=' . $data->id) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="Delete">
//                        <i class="la la-trash"></i>
//                    </a>';
//                })
//                ->setHeaderAttributes('kode_nota', 'table-header-cell', 'text-align: right; font-size: 14px; background-color:blue')
                ->addAction(route('pembelian.detail'), 'id', 'la la-edit')
                ->addAction(route('olap.print_out.view', ['print' => Constant::PrintPembelian]), 'id', 'la la-print', '')
                ->addAction(route('pembelian.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.procurement.pembelian.list');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        $message = [
//            'trans_date.required' => 'Date is required.',
//            'beone_supplier.required' => 'Supplier is required.',
//            'beone_po_import_details.required' => 'Keterangan is required.',
//            'keterangan.min' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [
            'supplier' => 'required',
            'keterangan' => 'max:100',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $status = 'Sukses ';
            $message = 'Pembelian Berhasil Dibuat';

            $data = new Pembelian();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Pembelian::with(['supplier'])->find($uid);
                $message = 'PO Import Berhasil Diedit';
            }
            if (!$uid) {
                $tgl = Carbon::createFromDate($inputs->input('tgl'));;
                $data->tgl = $tgl;
                $data->kode_nota = Helper::getTransNo('PB', $tgl, Pembelian::class, 'kode_nota');
                $data->created_by = Auth::id();
            }
            $supplier = $inputs->input('supplier');
            $keterangan = $inputs->input('keterangan');
            $grand_total = $inputs->input('grand_total');

            $pembelian_details = [];
            $detail = $inputs->input('pembelian_details');
            foreach ($detail as $key => $value) {
                if ($value['barang']->id <> '') {
                    $pembelian_detail = [
                        'id' => $value['id'],
                        'barang_id' => $value['barang']->id,
                        'qty' => $value['qty'],
                        'harga_satuan' => $value['harga_satuan'],
                        'subtotal' => $value['qty'] * $value['harga_satuan']
                    ];
                    $pembelian_details[] = $pembelian_detail;
                }
            }
            $data->supplier_id = $supplier->id;
            $data->keterangan = $keterangan;
            $data->grand_total = $grand_total;
            $data->updated_by = Auth::id();
            $data->save();
            Helper::syncMany($data->pembelian_details(), $pembelian_details, 'id');

            DB::commit();
            return Helper::redirect('pembelian.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Pembelian Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = Pembelian::find($uid);
        if ($data) {
            $data->pembelian_details()->update(['qty' => 0]);
            $data->delete();
        }
        return Helper::redirect('pembelian.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
