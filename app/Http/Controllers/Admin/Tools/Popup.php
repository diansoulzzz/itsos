<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Popup extends Controller
{
    public function getUserCabang(Request $request)
    {
        $users = User::with(['users_company'])->find(Auth::id());
        return $users->users_company;
    }

    public function postUserCabang(Request $request)
    {
        $users = User::with(['users_company'])->find(Auth::id());
        $users->active_company_id = $request->input('active_company_id');
        $users->save();
        return Helper::redirect('home', Constant::AlertPrimary, Constant::TitleSuccess, 'Berhasil Masuk atau Ganti Cabang');
    }
}
