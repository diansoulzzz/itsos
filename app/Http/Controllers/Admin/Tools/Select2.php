<?php

namespace App\Http\Controllers\Admin\Tools;

use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Barang;
use App\Models\BarangKategori;
use App\Models\Mekanik;
use App\Models\Pelanggan;
use App\Models\Satuan;
use App\Models\Supplier;
use App\Models\Unit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class Select2 extends Controller
{
    public static function genS2Value($id, $text)
    {
        $record = [
            'id' => $id,
            'text' => $text,
        ];
        return [
            'id' => json_encode($record),
            'text' => $text,
        ];
    }

    public function checkWithAll(Request $inputs, $items)
    {
        if ($inputs->has('with_all')) {
            $items = collect($items);
            $all = [
                'id' => '%',
                'text' => 'ALL',
            ];
            $all = [
                'id' => json_encode($all),
                'text' => 'ALL'
            ];
            $items->push($all);
        }
        return $items;
    }

    public static function toSelect2($items)
    {
        $iq = [];
        foreach ($items as $item) {
            $ar = [
                'id' => $item->id,
                'text' => $item->text,
            ];
            $iq[] = [
                'id' => json_encode($ar),
                'text' => $item->text
            ];
        }
        return $iq;
    }

    public function genSelect(Request $request, $model, $columns, $conditions = [])
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            $items = $model::where(function ($query) use ($q, $columns) {
                $isFirst = true;
                foreach ($columns as $column) {
                    if ($isFirst) {
                        $query->where($column, 'like', $q);
                        $isFirst = false;
                    }
                    $query->orwhere($column, 'like', $q);
                }
            });
            if ($conditions) {
                $items = $items->where([$conditions]);
            }
            $items = $items->get();
            $items = collect($items)->map(function ($item, $key) {
                $record = [
                    'id' => $item->id,
                    'text' => $item->s2_text,
                ];
                return [
                    'id' => json_encode($record),
                    'text' => $item->s2_text,
                ];
            });
            $items = $this->checkWithAll($inputs, $items);
            $total_count = $items->count();
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function barangKategori(Request $request)
    {
        return $this->genSelect($request, BarangKategori::class, ['id', 'nama']);
    }

    public function mekanik(Request $request)
    {
        return $this->genSelect($request, Mekanik::class, ['id', 'nama']);
    }

    public function unit(Request $request)
    {
        return $this->genSelect($request, Unit::class, ['id', 'nama']);
    }

    public function pelanggan(Request $request)
    {
        return $this->genSelect($request, Pelanggan::class, ['id', 'nama']);
    }

    public function supplier(Request $request)
    {
        return $this->genSelect($request, Supplier::class, ['id', 'nama']);
    }

    public function barang(Request $request)
    {
        return $this->genSelect($request, Barang::class, ['id', 'nama']);
    }

    public function satuanItem(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 1) {
            $q = '%' . $q . '%';
            if ($inputs->has('barang')) {
                $barang = $inputs->get('barang');
                $query = "select s.id, concat(s.rasio,' (', s.kode,')') as text
                from satuan s
                where s.item_id = '$barang->id' and (s.keterangan like '$q' or s.kode like '$q')";
            } else {
                return '';
            }
            $items = DB::select(DB::raw($query));
            $items = self::toSelect2($items);
            $total_count = count($items);
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }

    public function outstandingPO(Request $request)
    {
        $inputs = Helper::merge($request);
        $q = $inputs->input('q');
        $total_count = 0;
        $items = [];
        if (strlen($q) > 2) {
            $q = '%' . $q . '%';
            if (!$inputs->has('multiple')) {
                $items = BeonePoImportHeader::with(['beone_po_import_details'])
                    ->doesntHave('beone_import_headers')
                    ->doesntHave('beone_po_import_details.beone_import_details')
                    ->where('purchase_no', 'ilike', $q)
                    ->orWhere('keterangan', 'ilike', $q)
                    ->get();
                $total_count = $items->count();
            } else {
                if (!$inputs->has('beone_supplier')) {
                    return '';
                }
                $beone_supplier = $inputs->get('beone_supplier');
                $query = "select pid.purchase_detail_id                          as id,
                       concat(pih.purchase_no,' - ', pih.keterangan, ' (', i.item_code, ')') as text,
                       si.satuan_id,
                       concat(si.rasio, ' (', si.satuan_code, ')')     as satuan_text
                from beone_po_import_detail pid,
                     beone_item i,
                     beone_po_import_header pih,
                     beone_satuan_item si
                where pid.item_id = i.item_id
                  and i.item_id = si.item_id
                  and pid.purchase_header_id = pih.purchase_header_id
                  and (i.nama ilike '$q' or i.item_code ilike '$q' or pih.purchase_no ilike '$q' or pih.keterangan ilike '$q')
                and pih.supplier_id = '$beone_supplier->id';";

                $items = DB::select(DB::raw($query));
                $iq = [];
                foreach ($items as $item) {
                    $ar_satuan_def = [
                        'id' => $item->satuan_id,
                        'text' => $item->satuan_text,
                    ];
                    $satuan_def = [
                        'id' => json_encode($ar_satuan_def),
                        'text' => $item->satuan_text
                    ];
                    $ar = [
                        'id' => $item->id,
                        'text' => $item->text,
                        'satuan_def' => $satuan_def
                    ];
                    $iq[] = [
                        'id' => json_encode($ar),
                        'text' => $item->text
                    ];
                }
                $items = $iq;
                $total_count = count($iq);
            }
        }
        $result = [
            'total_count' => $total_count,
            'items' => $items,
        ];
        return response()->json($result);
    }
}
