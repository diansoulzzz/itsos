<?php

namespace App\Http\Controllers\Admin\Project;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\OrderKerja;
use App\Traits\FileUploadTraits;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AWorkOrderUploadController extends Controller
{
    use FileUploadTraits;

    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = OrderKerja::with(['pelanggan', 'mekanik', 'order_kerja_fotos'])->findOrFail($uid);
            return view('admin.menus.project.work_order_upload.detail', compact('detail'));
        }
        return Helper::redirect('work_order.list', Constant::AlertWarning, Constant::TitleWarning, 'Order Kerja Tidak Ditemukan');
    }

//    public function indexList(Request $request)
//    {
//        if ($request->ajax()) {
//            $query = 'select ok.id, ok.kode_nota as Nota, CAST(ok.tgl as DATE) as Tanggal, ok.masalah as Keluhan, p.nama as Pelanggan, ok.no_plat as No_polisi, ok.no_rangka as No_rangka, m.nama as Mekanik, ok.status from order_kerja ok
//                        join pelanggan p on ok.pelanggan_id = p.id
//                        join mekanik m on ok.mekanik_id = m.id
//                        where ok.deleted_at is null';
//            $actions =
//                '<a role="button" href="javascript:void(0);" data-url="' . route('work_order.upload.detail') . '" data-field-name="id" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
//                    <i class="la la-cloud-upload"></i>
//                </a>
//                <a role="button" href="javascript:void(0);" data-url="' . route('work_order.detail') . '" data-field-name="id" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
//                    <i class="la la-edit"></i>
//                </a>
//                <a role="button" href="javascript:void(0);" data-url="' . route('work_order.delete') . '" data-field-name="id" data-title="Yakin Hapus?" data-type="dialog" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
//                    <i class="la la-trash"></i>
//                </a>';
//            $result = Helper::generateResult($request, $query, $actions);
//            return response()->json($result)->setCallback($request->input('callback'));
//        }
//        return view('admin.menus.project.work_order.list');
//    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        if ($request->has('xfile')) {
            return $this->uploadFile($request);
        }

        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'id' => 'required',
            'fileUrls' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $invalid = $this->isInvalid($uid);
            if ($invalid) {
                return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
            }
            $data = OrderKerja::find($uid);
            $message = 'Data Bukti Order Kerja Berhasil Upload';
            $order_kerja_fotos = [];
            $detail = $inputs->input('fileUrls');
            foreach ($detail as $key => $value) {
                $order_kerja_foto = [
                    'id' => '',
                    'foto_url' => $this->moveUploadFile($value, 'work-order/' . $uid . '/'),
//                    'foto_url' => $value,
                    'created_by' => Auth::id(),
                    'updated_by' => Auth::id()
                ];
                $order_kerja_fotos[] = $order_kerja_foto;
            }
            $data->order_kerja_fotos()->createMany($order_kerja_fotos);
//            Helper::syncMany($data->order_kerja_fotos(), $order_kerja_fotos, 'id');
            DB::commit();
            return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        OrderKerja::find($uid)->delete();

        return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
