<?php

namespace App\Http\Controllers\Admin\Project;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\OrderKerja;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

class AWorkOrderController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = OrderKerja::with(['pelanggan', 'mekanik'])->find($uid);
            return view('admin.menus.project.work_order.detail', compact('detail'));
        }
        return view('admin.menus.project.work_order.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'select ok.id, ok.kode_nota as Nota, CAST(ok.tgl as DATE) as Tanggal, ok.masalah as Keluhan, p.nama as Pelanggan, ok.no_plat as No_polisi, ok.no_rangka as No_rangka, m.nama as Mekanik, ok.status from order_kerja ok
                        join pelanggan p on ok.pelanggan_id = p.id
                        join mekanik m on ok.mekanik_id = m.id
                        where ok.deleted_at is null';
            $grid = new Grid($request, $query);
            $grid
                ->addColumn('Progress', function ($data) {
                    $gridHelper = new GridHelper($data);
                    return $gridHelper
                        ->if($data->status, '==', 'BARU', function ($data) {
                            return '<a href="' . route('work_order.status', ['id' => $data->id, 'status' => $data->status]) . '" class="btn btn-primary prevent-dialog" data-sw-title="SET DIPERBAIKI?">
                                SET DIPERBAIKI
                            </a>';
                        })->if($data->status, '==', 'SEDANG DIPERBAIKI', function ($data) {
                            return '<a href="' . route('work_order.status', ['id' => $data->id, 'status' => $data->status]) . '" class="btn btn-primary prevent-dialog" data-sw-title="SET SELESAI?">
                                SET SELESAI
                            </a>';
                        })->getResult();
                })
                ->addColumn('FPB', function ($data) {
                    return '<a href="' . route('work_order.barang.detail', ['id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                                <i class="la la-plus"></i>
                            </a>
                            <a href="' . route('olap.print_out.view', ['id' => $data->id, 'print' => Constant::PrintOrderBarang]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                                <i class="la la-print"></i>
                            </a>';
                })
//                ->addAction(route('work_order.barang.detail'), 'id', 'la la-plus')
//                ->addAction(route('work_order.barang.detail'), 'id', 'la la-plus', '', '', '', function ($data, $template) {
//                    $gridHelper = new GridHelper($data);
//                    return $gridHelper
//                        ->if($data->status, '==', 'BARU', function ($data) use ($template) {
//                            return $template;
//                        })->getResult();
//                })
                ->addAction(route('olap.print_out.view', ['print' => Constant::PrintOrderKerja]), 'id', 'la la-print', '')
                ->addAction(route('work_order.upload.detail'), 'id', 'la la-upload')
                ->addAction(route('work_order.detail'), 'id', 'la la-edit')
//                ->addAction(route('olap.print_out.view', ['print' => Constant::PrintOrderBarang]), 'id', 'fas fa-print', '')
                ->addAction(route('work_order.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog');
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.project.work_order.list');
    }

    public function setStatus(Request $request)
    {
        $uid = $request->input('id');
        $status = $request->input('status');
        $message = '';
        if ($status == 'BARU') {
            $message = 'SEDANG DIPERBAIKI';
        } else if ($status == 'SEDANG DIPERBAIKI') {
            $message = 'SELESAI';
        } else {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, 'invalid status');
        }
        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        $data = OrderKerja::find($uid);
        $data->status = $message;
        $data->save();
        $message = $data->kode_nota . " STATUS: " . $message;
        return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'mekanik' => 'required',
            'pelanggan' => 'required',
            'no_plat' => 'required',
            'no_rangka' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Data Order Kerja Berhasil Tersimpan';

            $data = new OrderKerja();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = OrderKerja::find($uid);
                $message = 'Data Order Kerja Berhasil Diedit';
            }
            if (!$uid) {
                $tgl = Carbon::createFromDate($inputs->input('tgl'));;
                $data->tgl = $tgl;
                $data->kode_nota = Helper::getTransNo('OD', $tgl, OrderKerja::class, 'kode_nota');
            }
            $data->pelanggan_id = $inputs->input('pelanggan')->id;
            $data->mekanik_id = $inputs->input('mekanik')->id;
            $data->no_plat = $inputs->input('no_plat');
            $data->no_rangka = $inputs->input('no_rangka');
            $data->no_lambung = $inputs->input('no_lambung');
            $data->masalah = $inputs->input('masalah');
//            $data->status = $inputs->input('status');
            $data->status = 'BARU';


//            return $data;
            $data->save();

            DB::commit();
            return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        OrderKerja::find($uid)->delete();

        return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
