<?php

namespace App\Http\Controllers\Admin\Project;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\OrderKerja;
use App\Models\OrderKerjaBarang;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AWorkOrderBarangController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = OrderKerja::with(['order_kerja_barangs.barang'])->find($uid);
            return view('admin.menus.project.work_order_barang.detail', compact('detail'));
        }
        return Helper::redirect('work_order.list', Constant::AlertWarning, Constant::TitleWarning, 'Order Kerja Tidak Ditemukan');
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        $message = [
//            'trans_date.required' => 'Date is required.',
//            'beone_supplier.required' => 'Supplier is required.',
//            'beone_po_import_details.required' => 'Keterangan is required.',
//            'keterangan.min' => 'Keterangan maximal 100 character.',
        ];

        $this->validate($request, [

        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();

        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $tgl = Carbon::now();
            $created_by = Auth::id();
            $status = 'Sukses ';
            $message = 'Barang Berhasil Ditambahkan';

            $data = OrderKerja::find($uid);

            $order_kerja_barangs = [];
            $detail = $inputs->input('order_kerja_barangs');
            foreach ($detail as $key => $value) {
                if ($value['barang']->id <> '') {
                    $order_kerja_barang = [
                        'id' => $value['id'],
                        'barang_id' => $value['barang']->id,
                        'tgl' => $tgl,
                        'created_by' => $created_by,
                        'qty' => $value['qty']
                    ];
                    $order_kerja_barangs[] = $order_kerja_barang;
                }
            }

            Helper::syncMany($data->order_kerja_barangs(), $order_kerja_barangs, 'id');

            DB::commit();
            return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }
}
