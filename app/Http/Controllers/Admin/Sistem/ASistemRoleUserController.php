<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\BeoneModul;
use App\Models\BeoneRolesUser;
use App\Models\Modul;
use App\Models\Report;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemRoleUserController extends Controller
{
    public function indexDetail(Request $request)
    {
        if ($request->ajax()) {
            $roles_id = $request->input('id');
            $query = "select ma.id, m.modul, ma.access, ifnull(rma.id, 0) as granted
            from modul m
            join modul_access ma on m.id = ma.modul_id
            left join roles_modul_access rma on ma.id = rma.modul_access_id and rma.roles_id = '$roles_id'";
            $result = DB::select($query);
            return response()->json($result)->setCallback($request->input('callback'));
        }
        $uid = $request->input('id');
        if ($uid) {
            $detail = Role::with(['moduls'])->find($uid);
//            $all_ids = $detail->moduls->pluck('id');
//            $m_ids = $all_ids->filter(function ($value, $key) {
//                return (strpos($value, 'R') !== true);
//            });
//            $r_ids = $all_ids->filter(function ($value, $key) {
//                return (strpos($value, 'R') !== false);
//            })->map(function ($item, $key) {
//                return str_replace("R", "", $item);
//            });
            $all_modul = Modul::whereNotIn('id', $detail->moduls->pluck('id'))->where('is_hidden', false)->get();
//            $all_report = Report::whereNotIn('id', $r_ids)->get();
//            $all_modul = $all_modul->merge($all_report);

            $all_modul = Helper::groupTreeMenu(collect($all_modul)->toArray());
            $act_modul = Helper::groupTreeMenu(collect($detail->moduls)->toArray());
            return view('admin.menus.sistem.role_user.detail', compact('detail', 'all_modul', 'act_modul'));
        }
        $all_modul = Modul::where('is_hidden', false)->get();
        $all_modul = Helper::groupTreeMenu(collect($all_modul)->toArray());
        $act_modul = [];
        return view('admin.menus.sistem.role_user.detail', compact('all_modul', 'act_modul'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select r.id, r.nama_role as Nama_Role, r.keterangan from roles r
                        where r.deleted_at is null;';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('role_user.detail'), 'id', 'la la-edit', '')
                ->addAction(route('role_user.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.sistem.role_user.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'nama_role' => 'required|min:3|max:100',
            'keterangan' => 'required|min:3|max:100',
            'moduls' => 'required',
        ], $message);

        $inputs = Helper::merge($request);

//        return $inputs;
        try {
            $uid = $inputs->input('id');
            $message = 'Role User Berhasil Dibuat';

            $data = new Role();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Role::find($uid);
                $message = 'Role User Berhasil Diedit';
            }

            $data->nama_role = $inputs->input('nama_role');
            $data->keterangan = $inputs->input('keterangan');

            $roles_moduls = Helper::ungroupTreeMenu($inputs->input('moduls'));

            $moduls_access = $inputs->input('moduls_access');

            $data->save();
            $data->roles_moduls()->delete();
            $data->roles_moduls()->createMany($roles_moduls);

            $data->moduls_access()->sync($moduls_access);

            DB::commit();
            return Helper::redirect('role_user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function deleteData(Request $request)
    {
        $message = 'Role User Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }

        Role::find($uid)->delete();
        return Helper::redirect('role_user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function dataTable(Request $request)
    {
        $model = Role::query();
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($item) {
                return
                    '<a href="' . route('role_user.detail', ['id' => $item->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="View">
                        <i class="la la-edit"></i>
                    </a>
                    <a href="javascript:void(0);" data-url="' . route('role_user.delete', ['id' => $item->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md prevent-dialog" title="View">
                        <i class="la la-trash"></i>
                    </a>';
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function isInvalid($uid)
    {
        return false;
    }
}
