<?php

namespace App\Http\Controllers\Admin\Sistem;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Role;
use App\Models\User;
use App\Models\UsersCompany;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class ASistemUserController extends Controller
{
    public function indexDetail(Request $request)
    {
        if ($request->ajax()) {
            $users_id = $request->input('id');
            $query = "select c.id,c.cabang, c.nama,  ifnull(uc.id, 0) as granted
                        from company c
                         left join users_company uc on c.id = uc.company_id and uc.users_id =  '$users_id'";
            $result = DB::select($query);
            return response()->json($result)->setCallback($request->input('callback'));
        }
        $uid = $request->input('id');
        $role = Role::get();
        if ($uid) {
            $detail = User::with(['role'])->find($uid);
            $role = Role::get();
//            return $detail;
            return view('admin.menus.sistem.user.detail', compact('detail','role'));
        }
        return view('admin.menus.sistem.user.detail',compact('role'));
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'Select u.id, u.username, u.nama, r.nama_role from users u
                        join roles r on u.roles_id = r.id
                        where u.deleted_at is null';
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('user.detail'), 'id', 'la la-edit', '')
                ->addAction(route('user.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.sistem.user.list');
    }
    public function ganti_password(Request $request)
    {
        $user_id = auth()->id();
        $user = DB::table('users')
            ->select('username')
            ->where('id',$user_id)
            ->get();

//            User::select('username')->where('id',$user_id)->get();
//        return $user;
        return view('admin.menus.sistem.user.ganti_password',compact('user'));
    }
    public function updatePassword(Request $request)
    {
        $username=$request->input('username');
        $password=$request->input('currentpassword');
        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            if($request->input('newpassword') == $request->input('confnewpassword')){
                $user = User::find(Auth::id());
                $user->password = Hash::make($request->input('newpassword'));
                $user->save();
                Auth::logout();
                return redirect()->route('login');
            }else{
                $message="_______Password Baru tidak sama dengan Konfirmasi Password Baru";
                return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message);
            }
        }else{
            $message="_______Password Lama Tidak Cocok";
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $message);
        }
    }

    public function postDetail(Request $request)
    {
//        return $request->all();
        $uid = $request->input('id');
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'username' => 'required|min:3|max:100',
            'nama' => 'required|min:3|max:100',
            'password' => 'nullable|min:5|required_with:validasi_password|same:validasi_password',
            'validasi_password' => 'nullable|min:5',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        try {
            $uid = $inputs->input('id');
            $message = 'User Berhasil Dibuat';

            $data = new User();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = User::find($uid);
                $message = 'User Berhasil Diedit';
            }
            $username = $inputs->input('username');
            $nama = $inputs->input('nama');
            $roles_id = $inputs->input('roles_id');
            $password = $inputs->input('password');
            $users_company = $inputs->input('users_company');
            if ($password) {
                $data->password = Hash::make($password);
            }
            $data->username = $username;
            $data->nama = $nama;
            $data->roles_id = $roles_id;
            $data->api_token = Str::random(100);
            $data->save();

            $data->users_company()->sync($users_company);

            return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }

    }

    public function deleteData(Request $request)
    {
        $message = 'User Berhasil Dihapus';
        $uid = $request->input('id');

        $invalid = $this->isInvalid($uid);
        if ($invalid) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
        }
        User::find($uid)->delete();

        return Helper::redirect('user.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function IsInvalid($uid)
    {
        return false;
    }
}
