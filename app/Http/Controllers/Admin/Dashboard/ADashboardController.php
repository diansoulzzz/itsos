<?php

namespace App\Http\Controllers\Admin\Dashboard;

use App\Constants\Constant;
use App\Helpers\Chart;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Http\Controllers\Controller;
use App\Models\Company;
use App\Models\Dashboard;
use Auth;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Validator;

class ADashboardController extends Controller
{
    public function printTest(Request $request)
    {
        $data = [
            'title' => 'LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN',
            'company' => Company::first(),
            'data' => BeoneImportHeader::with(['beone_supplier', 'beone_import_details.beone_item'])->get(),
        ];
        // return $data;
        $data = BeoneImportHeader::with(['beone_import_details.beone_item'])->get();
        $pdf = PDF::loadView('pdf.report-pemasukan-barang', $data)->setPaper('a4', 'landscape');
        return $pdf->stream();
    }

    public function getRouteList()
    {
        $getRouteCollection = Route::getRoutes();
        $list = [];
        foreach ($getRouteCollection as $route) {
            if ($route->getName() == '') continue;
            if (strpos($route->getName(), 's2') !== false) {
                continue;
            }
            if (strpos($route->getName(), 's3') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'dele') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'login') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'logout') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'print') !== false) {
                continue;
            }
            if (strpos($route->getName(), 'load') !== false) {
                continue;
            }
            $list[] = $route->getName();
        }
        $list = collect(array_unique($list));
        $x = 4;
        foreach ($list as $i => $route) {
            echo '<br>' . "select " . ($x++) . " as modul_id, '" . ucwords(preg_replace('/[^A-Za-z0-9\-]/', ' ', $route)) . "' as modul, '" . $route . "' as url, null as icon_class union all";
        }
        return '';
    }

    public function test()
    {
//        return Auth::user();
//        return $this->getRouteList();
//        $user = BeoneUser::with(['beone_roles_users.beone_moduls'])->find(2);
//        $moduls = $user->beone_roles_users->beone_moduls;
//        $g_sidebar = Helper::sidebar(request(), collect($moduls)->toArray());
//        return $g_sidebar;
//        return $user;
//        $moduls = collect($moduls)->toArray();
//        return $moduls;
//        $sidebar = Helper::sidebar($request, $moduls);
    }

    public function index(Request $request)
    {
//        $data = new Chart($request, "
//        select 'a' as categories, 'series' as series, '1' as value
//union
//select 'a' as categories, 'series' as series, '2' as value
//union
//select 'a' as categories, 'series' as series, '3' as value
//union
//select 'b' as categories, 'tes2' as series, '1' as value
//union
//select 'b' as categories, 'tes2' as series, '2' as value
//union
//select 'b' as categories, 'tes2' as series, '3' as value;", Constant::ReportBar);
//        return $data->get();

        if ($request->ajax()) {
            $uid = $request->input('i');
            $detail = Dashboard::with(['role', 'report'])->where('roles_id', Auth::user()->roles_id)->find($uid);
            if ($detail->report->type == Constant::ReportGeneral) {
                $data = new Grid($request, $detail->report->query);
                $result = $data->get();
            } else if ($detail->report->type == Constant::ReportBar || $detail->report->type == Constant::ReportLine || $detail->report->type == Constant::ReportPie) {
                $data = new Chart($request, $detail->report->query, $detail->report->type);
                $result = $data->get();
            }
            return response()->json($result)->setCallback($request->input('callback'));
        }
        $dashboard = Dashboard::with(['role', 'report'])->where('roles_id', Auth::user()->roles_id)->get();
        return view('admin.menus.dashboard.index', compact('dashboard'));
    }

}
