<?php

namespace App\Http\Controllers\Admin\Event;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Acara;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AAcaraController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Acara::find($uid);
            return view('admin.menus.event.acara.detail', compact('detail'));
        }
        return view('admin.menus.event.event.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select a.id, a.nama, a.lokasi, a.biaya_peratlet, cast(a.tgl_start as date) as Start, cast(a.tgl_end as date) as End, a.keterangan from acara a
                        where a.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('acara.detail'), 'id', 'la la-edit', '')
                ->addAction(route('acara.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.event.acara.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'penerima' => 'required',
            'jenis_biaya' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs;
        try {
            //generate no_trip
            $q = DataMuatan::where('no_trip', 'like', 'L%')
                ->latest()
                ->first();

            $new_code = '';
            $last = '';
            if ($q == null) {
                $new_code = 'L1';
            } else {
                // $last = Str::substr($hasil,2,1);
                $hasil = $q->no_trip;
                $last = (int)(Str::substr($hasil, 1, 5)) + 1;
                $new_code = 'L' . $last;
            }
            $uid = $inputs->input('id');
            $message = 'Data Muatan Luar Berhasil Dibuat';

            $data = new DataMuatan();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = DataMuatan::find($uid);
                $message = 'Data Muatan Luar Berhasil Diedit';
            }
            $data->no_trip = $new_code;
            $data->kode_data = 'MUATAN_LUAR';
            $data->penerima = $inputs->input('penerima');
            $data->jenis_biaya = $inputs->input('jenis_biaya');
            $data->penerima = $inputs->input('penerima');
            $tanggal = Carbon::createFromDate($inputs->input('tanggal'));;
            $data->tanggal = $tanggal;
            $data->penyewa = $inputs->input('penyewa');
            $data->muatan = $inputs->input('muatan');
            $data->no_pol = $inputs->input('no_pol');
            $data->jumlah = $inputs->input('jumlah');
            $data->nilai_awal = $inputs->input('nilai_awal');
            $data->asal = $inputs->input('asal');
            $data->tujuan = $inputs->input('tujuan');
            $data->users_id = auth()->id();
            $data->save();
            return Helper::redirect('muatan_luar.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Item Berhasil Dihapus';
        $uid = $request->input('id');

        Acara::find($uid)->delete();

        return Helper::redirect('acara.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }

    public function showPeserta(Request $request)
    {
        if ($request->ajax()){
            $uid = $request->input('id');
            $query = "select ks.id, a.nama as acara, k.kategori, k2.kategori_kelas, k2.kelas, p.nama as pengkot, a2.nama_lengkap, j.nama as juara from kategori_acara ks
                        join acara a on ks.acara_id = a.id
                        join kategori k on ks.kategori_id = k.id
                        join kelas k2 on ks.kelas_id = k2.id
                        join pengkot p on ks.pengkot_id = p.id
                        join atlet a2 on ks.atlet_id = a2.id
                        join juara j on ks.juara_id = j.id
                        where ks.acara_id = '". $uid ."'";
            $result = DB::select($query);

            return response()->json($result)->setCallback($request->input('callback'));
        }
//        return $uid;
        return view('admin.menus.event.acara.participant');
    }
}
