<?php

namespace App\Http\Controllers\Admin\Event;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\GridHelper;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Imports\AcaraDetailGuestImport;
use App\Models\Acara;
use App\Models\AcaraDetailGuest;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use phpDocumentor\Reflection\Types\Collection;
use Validator;
use Yajra\DataTables\DataTables;

class AAcaraGuestController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $aid = $request->input('aid');
        if ($uid) {
            $detail = AcaraDetailGuest::find($uid);
            return view('admin.menus.event.acara.detail_guest.detail', compact('detail'));
        }
        return view('admin.menus.event.acara.detail_guest.detail', compact('aid'));
    }

    public function sendEmail(Request $request)
    {
        $uid = $request->input('id');
        $acara = Acara::with(['pelanggan', 'acara_detail_guests_email.acara.pelanggan'])->find($uid);
        foreach ($acara->acara_detail_guests_email as $data) {
            $to_name = $data->nama;
            $to_email = $data->email;
            Mail::send('emails.invitation', compact('data'), function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name);
                $message->subject('3Vites Invitation');
                $message->from('three.vites@gmail.com', '3Vites');
            });
            $guest = AcaraDetailGuest::find($data->id);
            $guest->email_at = Carbon::now();
            $guest->save();
        }
        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, 'Invitation Sent');
    }

    public function import(Request $request)
    {
        if ($request->hasFile('file')) {
            $acara = Acara::findOrFail($request->input('id'));
            $file = $request->file('file');
            $dateImport = Excel::toArray(new AcaraDetailGuestImport($acara), $file);
            $dateImport = collect($dateImport)->first();
            foreach ($dateImport as $val) {
                AcaraDetailGuest::updateOrCreate(
                    ['acara_id' => $acara->id, 'nama' => $val['nama'], 'no_telp' => $val['no_telp'], 'qrcode' => Str::random(200)],
                    ['is_vip' => $val['is_vip']]
                );
            }
        }
        return redirect()->back();
    }


    public function indexList(Request $request)
    {
        $uid = $request->input('id');
        if ($request->ajax()) {
            $query = "select a.id as hid, adg.id, adg.nama as guest, adg.email, adg.no_telp, adg.is_vip as vip, adg.attend_at as attend_at, adg.email_at as email_at
                        from acara_detail_guest adg
                        join acara a on adg.acara_id = a.id
                        join pelanggan p on a.pelanggan_id = p.id
                        where  a.id = '" . $uid . "' and a.deleted_at is null";
            $grid = new Grid($request, $query);
            $grid->editColumn('vip', function ($data) {
                $gridHelper = new GridHelper($data);
                return $gridHelper
                    ->if($data->vip, '==', 1, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">Yes</span>';
                    })
                    ->if($data->vip, '==', 0, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger kt-badge--rounded">No</span>';
                    })->getResult();
            });
            $grid->editColumn('attend_at', function ($data) {
                $gridHelper = new GridHelper($data);
                return $gridHelper
                    ->if($data->attend_at, '==', 1, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--success kt-badge--rounded">' . $data->attend_at . '</span>';
                    })
                    ->if($data->attend_at, '==', 0, function ($data) {
                        return '<span class="kt-badge kt-badge--inline kt-badge--pill kt-badge--danger kt-badge--rounded"></span>';
                    })->getResult();
            });
            $grid->addColumn('Card', function ($data) {
                return '<a href="' . route('olap.print_out.view', ['print' => 'Acara', 'hid' => $data->hid, 'id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                                <i class="la la-print"></i>
                            </a>';
            });
//            $grid->addAction(route('olap.print_out.view', ['print' => 'Acara', 'guest'=>]), 'id', 'la la-print', '');
            $grid->addAction(route('acara_guest.detail'), 'id', 'la la-edit', '');
            $grid->addAction(route('acara_guest.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog');
            $grid->setHidden(['hid']);
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        $detail = Acara::with(['pelanggan'])->find($uid);
        return view('admin.menus.event.acara.detail_guest.list', compact('detail'));
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Guest Berhasil Ditambahkan';

            $data = new AcaraDetailGuest();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = AcaraDetailGuest::find($uid);
                $message = 'Guest Berhasil Diupdate';
            } else {
                $data->acara_id = $inputs->input('aid');
            }

            $nama = $inputs->input('nama');
            $email = $inputs->input('email');
            $no_telp = $inputs->input('no_telp');
            $is_vip = $inputs->has('is_vip');

            $data->nama = $nama;
            $data->email = $email;
            $data->no_telp = $no_telp;
            $data->qrcode = Str::random(200);
            $data->is_vip = $is_vip;

//            return $request->all();

            $data->save();

            DB::commit();
            return Helper::redirect('acara_guest.list', Constant::AlertSuccess, Constant::TitleSuccess, $message, ['id' => $data->acara_id]);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Guest Berhasil Dihapus';
        $uid = $request->input('id');

        AcaraDetailGuest::find($uid)->delete();

        return Helper::redirect('', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
