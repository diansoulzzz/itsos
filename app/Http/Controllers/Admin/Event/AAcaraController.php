<?php

namespace App\Http\Controllers\Admin\Event;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Acara;
use App\Models\Klub;
use App\Models\Pengkot;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use Yajra\DataTables\DataTables;

class AAcaraController extends Controller
{
    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        if ($uid) {
            $detail = Acara::with(['pelanggan'])->find($uid);
            return view('admin.menus.event.acara.detail', compact('detail'));
        }
        return view('admin.menus.event.acara.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = "select a.id, cast(a.tgl as date) as tanggal_nota, a.kode_nota, a.waktu as tanggal_acara, a.lokasi, p.nama as customer
                        from acara a
                                 join pelanggan p on a.pelanggan_id = p.id
                        where a.deleted_at is null";
            $grid = new Grid($request, $query);
            $result = $grid
                ->addAction(route('acara.detail'), 'id', 'la la-edit', '')
                ->addAction(route('acara_guest.list'), 'id', 'fas fa-user-tie', '')
                ->addAction(route('acara_foto.upload.detail'), 'id', 'la la-upload')
//                ->addAction(route('acara.detail'), 'id', 'la la-print', '')
//                ->addAction(route('olap.print_out.view', ['print' => 'Acara']), 'id', 'la la-print', '')
                ->addAction(route('acara.delete'), 'id', 'la la-trash', '', 'Yakin Hapus?', 'dialog')
                ->addColumn('Card', function ($data) {
                    return '<a href="' . route('olap.print_out.view', ['print' => 'Acara', 'hid' => $data->id, 'hm']) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                                <i class="la la-edit"></i>
                            </a>' . '<a href="' . route('olap.print_out.view', ['print' => 'Acara', 'hid' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md btn-action">
                                <i class="la la-print"></i>
                            </a>';
                })
                ->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.event.acara.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
            'pelanggan' => 'required',
            'lokasi' => 'max:200',
        ];

        $this->validate($request, [
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Acara Berhasil Disimpan';

            $data = new Acara();
            $users = Auth::id();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Acara::with(['pelanggan'])->find($uid);
                $message = 'Acara Berhasil Diedit';
            }
            if (!$uid) {
                $tgl = Carbon::createFromDate($inputs->input('tgl'));
                $data->tgl = $tgl;
                $data->kode_nota = Helper::getTransNo('EV', $tgl, Acara::class, 'kode_nota');
                $data->created_by = $users;
            }
            $tgl_waktu = Carbon::createFromDate($inputs->input('waktu'));
            $pelanggan = $inputs->input('pelanggan');
            $lokasi = $inputs->input('lokasi');

            $data->waktu = $tgl_waktu;
            $data->pelanggan_id = $pelanggan->id;
            $data->lokasi = $lokasi;
            $data->updated_by = $users;

            $data->save();

            DB::commit();
            return Helper::redirect('acara.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function deleteData(Request $request)
    {
        $message = 'Acara Berhasil Dihapus';
        $uid = $request->input('id');

        Acara::find($uid)->delete();

        return Helper::redirect('acara.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
    }
}
