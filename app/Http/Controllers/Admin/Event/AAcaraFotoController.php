<?php

namespace App\Http\Controllers\Admin\Event;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Acara;
use App\Models\AcaraDetailFoto;
use App\Models\OrderKerja;
use App\Traits\FileUploadTraits;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Validator;

class AAcaraFotoController extends Controller
{
    use FileUploadTraits;

    public function indexDetail(Request $request)
    {
        $uid = $request->input('id');
        $enc_uid = Crypt::encryptString($uid);
        $auth = Auth::id();
        $enc_auth = Crypt::encryptString($auth);
        $copy_link = route('upload.detail', ['id' => $enc_uid, 'by' => $enc_auth]);
//        $copy_link = 'http://localhost:8090/upload?id=' . $enc_uid . '/by?id=' . $enc_auth;
//        return $copy_link;
        if ($uid) {
            $detail = Acara::with(['acara_detail_fotos'])->findOrFail($uid);
//            return $detail;
            return view('admin.menus.event.acara.detail_foto.detail', compact('detail', 'copy_link'));
        }
        return Helper::redirect('acara.list', Constant::AlertWarning, Constant::TitleWarning, 'Order Kerja Tidak Ditemukan');
    }

    public function indexUpload(Request $request)
    {
        $uid = $request->input('id');
        $uid = Crypt::decryptString($uid);

        $by = $request->input('by');
        $by = Crypt::decryptString($by);

        if ($uid) {
            $detail = Acara::with(['acara_detail_fotos'])->findOrFail($uid);
            return view('admin.menus.event.acara.detail_foto.upload', compact('detail', 'by'));
        }
        return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, 'Order Kerja Tidak Ditemukan', compact('by'));
    }

    public function isInvalid($uid)
    {
        return false;
    }

    public function postDetail(Request $request)
    {
        if ($request->has('xfile')) {
            return $this->uploadFile($request);
        }

        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'id' => 'required',
            'fileUrls' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
//        return $inputs->all();
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $invalid = $this->isInvalid($uid);
            if ($invalid) {
                return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
            }
            $data = Acara::find($uid);
            $message = 'Data Bukti Order Kerja Berhasil Upload';
            $acara_detail_fotos = [];
            $detail = $inputs->input('fileUrls');
            $user_id = $inputs->input('by');

            $route = '';
            if (Auth::check()) {
                $user_id = Auth::id();
                $route = 'acara.list';
            }

            foreach ($detail as $key => $value) {
                $acara_detail_foto = [
                    'id' => '',
                    'foto_url' => $this->moveUploadFile($value, 'acara-foto/' . $uid . '/'),
//                    'foto_url' => $value,
                    'created_by' => $user_id,
                    'updated_by' => $user_id,
                ];
                $acara_detail_fotos[] = $acara_detail_foto;
            }
            $data->acara_detail_fotos()->createMany($acara_detail_fotos);
//            Helper::syncMany($data->order_kerja_fotos(), $order_kerja_fotos, 'id');
            DB::commit();

            return Helper::redirect($route, Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

//    public function deleteData(Request $request)
//    {
//        $message = 'Item Berhasil Dihapus';
//        $uid = $request->input('id');
//
//        $invalid = $this->isInvalid($uid);
//        if ($invalid) {
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
//        }
//
//        OrderKerja::find($uid)->delete();
//
//        return Helper::redirect('work_order.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
//    }
}
