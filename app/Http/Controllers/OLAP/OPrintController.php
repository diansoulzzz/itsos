<?php

namespace App\Http\Controllers\OLAP;

use App\Constants\Constant;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\Acara;
use App\Models\Company;
use App\Models\PrintOut;
use DB;
use Illuminate\Http\Request;
use Vendor\Package\Stimulsoft\Classes\StiResult as CoreResult;
use Vendor\Package\Stimulsoft\CoreHandler;
use Vendor\Package\Stimulsoft\CoreHelper;

class OPrintController extends Controller
{
    public function index(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->handle($request);
        }
        $data = $this->getData($request);
        if (!$data) {
            return Helper::getResponse([], 'No Print Out Data found', Constant::ResponseError);
//            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, 'No Print Out Data found');
        }
        $designer = $this->getPrintDesign($request);
        $options = CoreHelper::createOptions();
        $options->handler = $request->fullUrl();
        $options->timeout = 30;
        $core = new CoreHelper();
        $core->setOptions($options);
        $is_design = $request->has('hm');
        $data = [
            'is_design' => $is_design,
            'core' => $core,
            'designer' => $designer,
            'data' => $data
        ];
        return Helper::getResponse($data);
//        return view('admin.menus.olap.print_out.generate', compact('is_design', 'core', 'designer', 'data'));
    }

    public function getPrintDesign(Request $request)
    {
        $designer = "";
        if ($request->input('print') == Constant::PrintAcara) {
            $print_out = Acara::find($request->input('hid'));
            $print_out->makeVisible('design');
            $designer = $print_out->design;
        } else {
            $print_out = PrintOut::firstOrCreate(['nama' => $request->input('print')]);
            $designer = ($print_out->design) ? $print_out->design : $print_out->design_def;
        }
        return $designer;
    }

    public function getData(Request $request)
    {
        $printOut = $request->input('print');
        $uid = $request->input('id');
        $hid = $request->input('hid');
        $data = [];

        switch ($printOut) {
            case Constant::PrintInvoice:
                $query = "select p.id, p.approve,p.deskripsi, h.no_trip, h.tanggal,p.data_invoice_header_id, h.no_invoice, h.kepada, h.alamat as alamat_invoice, i.price, i.description, h.keterangan, h.total_tagihan, h.ppn, h.penyewa, h.company_id as company, u.username
                                   from data_invoice_print p
                                     join data_invoice_header h on p.data_invoice_header_id = h.id
                                     join invoice_detail i on h.id = i.invoice_header_id
                                     join users u on p.users_id = u.id
                                     join company c on u.active_company_id = c.id
                            where p.data_invoice_header_id = '" . $uid . "'";
                $data = DB::select(DB::raw($query));
                break;
            case Constant::PrintPembelian:
                $query = "select p.kode_nota,
                       p.tgl,
                       s.nama as nama_supplier,
                       b.kode as kode_brg,
                       b.nama as nama_brg,
                       pd.qty,
                       pd.harga_satuan,
                       pd.subtotal,
                       p.grand_total,
                       p.keterangan
                from pembelian p
                         join pembelian_detail pd on p.id = pd.pembelian_id
                         join barang b on pd.barang_id = b.id
                         join supplier s on p.supplier_id = s.id
                where p.id = '" . $uid . "'";
                $data = DB::select(DB::raw($query));
                break;
            case Constant::PrintOrderKerja:
                $query = "select k.kode_nota, k.tgl, p.nama as nama_pelanggan, m.nama as nama_mekanik, k.masalah, k.status, ifnull(b.kode,'') as kode_brg, ifnull(b.nama,'') as nama_brg
                from order_kerja k
                         join mekanik m on k.mekanik_id = m.id
                         join pelanggan p on k.pelanggan_id = p.id
                         left join order_kerja_barang okb on k.id = okb.order_kerja_id
                         left join barang b on okb.barang_id = b.id
                where k.id  = '" . $uid . "'";
                $data = DB::select(DB::raw($query));
                break;
            case Constant::PrintAcara:
                $query = "select k.kode_nota, k.tgl, p.nama as nama_pelanggan, m.nama as nama_mekanik, k.masalah, k.status, ifnull(b.kode,'') as kode_brg, ifnull(b.nama,'') as nama_brg
                from order_kerja k
                         join mekanik m on k.mekanik_id = m.id
                         join pelanggan p on k.pelanggan_id = p.id
                         left join order_kerja_barang okb on k.id = okb.order_kerja_id
                         left join barang b on okb.barang_id = b.id
                where k.id  = '" . $hid . "'";
                $data = DB::select(DB::raw($query));
                break;
            default:
                break;
        }
        if (count($data) < 1) {
            return false;
        }
        $result = [
            'company' => Company::first(),
            'data' => $data,
        ];
        return $result;
    }

    public function handle(Request $request)
    {
        $handler = new CoreHandler();
        $handler->registerErrorHandlers();

        $handler->onBeginProcessData = function ($event) {
            $database = $event->database;
            $connection = $event->connection;
            $dataSource = $event->dataSource;
            $connectionString = $event->connectionString;
            $queryString = $event->queryString;
            return CoreResult::success();
        };

        $handler->onPrintReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onBeginExportReport = function ($event) {
            $settings = $event->settings;
            $format = $event->format;
            return CoreResult::success();
        };

        $handler->onEndExportReport = function ($event) {
            $format = $event->format;
            $data = $event->data;
            $fileName = $event->fileName;
            file_put_contents('reports/' . $fileName . '.' . strtolower($format), base64_decode($data));
            return CoreResult::success("Export OK. Message from server side.");
        };
        $handler->onEmailReport = function ($event) {
            $event->settings->from = "******@gmail.com";
            $event->settings->host = "smtp.gmail.com";
            $event->settings->login = "******";
            $event->settings->password = "******";
        };

        $handler->onDesignReport = function ($event) {
            return CoreResult::success();
        };

        $handler->onCreateReport = function ($event) {
            $fileName = $event->fileName;
            return CoreResult::success();
        };

        $handler->onSaveReport = function ($event) use ($request) {
            $report = $event->report;
            $reportJson = $event->reportJson;
            $fileName = $event->fileName;
            if ($request->input('print') == Constant::PrintAcara) {
                $acara = Acara::find($request->input('hid'));
//                $acara->makeVisible('design');
                $acara->design = $reportJson;
                $acara->save();
            } else {
                PrintOut::updateOrCreate(
                    ['nama' => $request->input('print')],
                    ['design' => $reportJson]
                );
            }
            return CoreResult::success("Save Report OK: " . $fileName);
        };

        $handler->onSaveAsReport = function ($event) {
            return CoreResult::success();
        };

        return $handler->process();
    }
}
