<?php

namespace App\Http\Controllers\OLAP;

use App\Constants\Constant;
use App\Helpers\Grid;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Models\DataMuatan;
use App\Models\Modul;
use App\Models\Report;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Validator;
use Yajra\DataTables\DataTables;

class OReportController extends Controller
{
    public function indexDetail(Request $request)
    {
        if ($request->has('submit') || $request->has('preview')) {
            return $this->preview($request, $request->input('code'), $request->input('name'), $request->input('query'), 0, $request->input('type'));
        }
        $uid = $request->input('id');
        if ($uid) {
            $detail = Report::with(['modul'])->find($uid);
            return view('admin.menus.olap.report.detail', compact('detail'));
        }
        return view('admin.menus.olap.report.detail');
    }

    public function indexList(Request $request)
    {
        if ($request->ajax()) {
            $query = 'select r.id, r.code, r.name, r.type from report r
                        where r.deleted_at is null';
            $grid = new Grid($request, $query);
            $grid
                ->addAction(route('olap.report.detail'), 'id', 'la la-edit');
            $result = $grid->get();
            return response()->json($result)->setCallback($request->input('callback'));
        }
        return view('admin.menus.olap.report.list');
    }

    public function postDetail(Request $request)
    {
        $message = [
            // 'name.required' => 'The email field is required.',
            // 'name.min' => 'Minimum length is 3',
        ];

        $this->validate($request, [
            'code' => 'required',
            'name' => 'required',
        ], $message);

        $inputs = Helper::merge($request);
        DB::beginTransaction();
        try {
            $uid = $inputs->input('id');
            $message = 'Report Berhasil Dibuat';
            $name = $request->input('name');

            $last = Modul::where('url', 'report')->orderByDesc('id')->first();

            $modul_dept = Modul::find($inputs->input('dept'));

            $modul_id = ($last ? $last->id + 10000 : 10001);

            $modul = new Modul();
            $modul->id = $modul_id;
            $data = new Report();
            if ($uid) {
                $invalid = $this->isInvalid($uid);
                if ($invalid) {
                    return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $invalid);
                }
                $data = Report::find($uid);
                $modul = Modul::find($data->modul_id);
                $modul_id = $modul->id;
                $message = 'Report Berhasil Diedit';
            }
            $modul->modul = $name;
            $modul->url = 'report';
            $modul->parent_id = $modul_dept->id;
            $modul->save();

            $data->code = $request->input('code');
            $data->name = $name;
            $data->type = $request->input('type');
            $data->query = $request->input('query');
            $data->modul_id = $modul_id;
            $data->save();

            DB::commit();
            return Helper::redirect('olap.report.list', Constant::AlertSuccess, Constant::TitleSuccess, $message);
        } catch (\Exception $e) {
            DB::rollback();
            return Helper::redirect('', Constant::AlertWarning, Constant::TitleWarning, $e->getMessage());
        }
    }

    public function dataTable(Request $request)
    {
        $model = Report::query()->with(['modul']);
        $datatable = Datatables::of($model)
            ->addColumn('action', function ($data) {
                $actions = "";
                $actions .= '<a href="' . route('olap.report.detail', ['id' => $data->id]) . '" class="btn btn-sm btn-clean btn-icon btn-icon-md" title="Edit">
                        <i class="la la-edit"></i>
                    </a>';
                return $actions;
            })
            ->escapeColumns([])
            ->make();
        return $datatable;
    }

    public function generateReport(Request $request)
    {
        $modul = Modul::findOrFail($request->input('i'));
        $report = Report::where('modul_id', $modul->id)->firstOrFail();
        return $this->preview($request, $report->code, $report->name, $report->query, $report->modul_id, $report->type);
    }

    public function preview(Request $request, $code, $name, $query, $modul_id, $type, $connection = Constant::DefConnection)
    {
        if ($type == Constant::ReportGeneral) {
            return $this->previewGrid($request, $code, $name, $query, $modul_id, $type, $connection);
        }
        return $this->previewChart($request, $code, $name, $query, $modul_id, $type, $connection);
    }

    public function previewChart(Request $request, $code, $name, $query, $modul_id, $type, $connection)
    {

    }

    public function previewGrid(Request $request, $code, $name, $query, $modul_id, $type, $connection)
    {
        $grid = new Grid($request, $query);
        $grid->setConnection($connection);
        $params = $grid->getParam();
        $result = [];
        if ($request->has('submit') || $request->has('i') || $request->has('preview') || count($params) <= 0) {
            $result = $grid->get();
        }
        $detail = [
            'code' => $code,
            'name' => $name,
            'type' => $type,
            'connection' => $connection,
            'modul_id' => $modul_id,
            'preview' => $request->has('preview'),
            'query' => $request->has('preview') ? $query : '',
        ];
        return view('admin.menus.olap.report.generate', compact('detail', 'params', 'result'));
    }
}
