import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/core/services/store";
import ApiService from "@/core/services/api.service";
import MockService from "@/core/mock/mock.service";
import {LOGOUT, VERIFY_AUTH} from "@/core/services/store/auth.module";
import {RESET_LAYOUT_CONFIG} from "@/core/services/store/config.module";

Vue.config.productionTip = false;

// Global 3rd party plugins
import "popper.js";
import "tooltip.js";
import PerfectScrollbar from "perfect-scrollbar";

window.PerfectScrollbar = PerfectScrollbar;
import ClipboardJS from "clipboard";

window.ClipboardJS = ClipboardJS;

// Vue 3rd party plugins
import i18n from "@/core/plugins/vue-i18n";
import vuetify from "@/core/plugins/vuetify";
import "@/core/plugins/portal-vue";
import "@/core/plugins/bootstrap-vue";
import "@/core/plugins/perfect-scrollbar";
import "@/core/plugins/highlight-js";
import "@/core/plugins/inline-svg";
import "@/core/plugins/apexcharts";
import "@/core/plugins/metronic";
import "@/core/plugins/vue-notification";
import "@mdi/font/css/materialdesignicons.css";

import "@/core/plugins/load-script";
import "@/core/plugins/kendo-ui-wrapper";
import "@/core/helpers/helper";

import {ValidationObserver, ValidationProvider, extend, localize} from "vee-validate";
import en from "vee-validate/dist/locale/en.json";
import * as rules from "vee-validate/dist/rules";
import {RouteLogin} from "@/router";

Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});
localize("en", en);
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);

// API service init
ApiService.init();

/*
// Remove this to disable mock API
MockService.init();
*/

router.beforeEach((to, from, next) => {
    // Ensure we checked auth before each page load.
    // console.log(to.name);
    // Promise.all([store.dispatch(VERIFY_AUTH)]).then(values => {
    //     next();
    // });\
    // if (to.name !== 'login') {
    Promise.all([store.dispatch(VERIFY_AUTH)]).then(values => {
        document.title = to.meta.title
        next();
    }).catch(values => {
        store.dispatch(LOGOUT).then(() => (router.push({name: RouteLogin})));
    })

    const {dispatch} = to.meta
    if (dispatch) store.dispatch(dispatch).then(() => (dispatch === LOGOUT ? router.push({name: RouteLogin}) : null));

    store.dispatch(RESET_LAYOUT_CONFIG);

    // Scroll page to top on every route change
    setTimeout(() => {
        window.scrollTo(0, 0);
    }, 100);
});

// Vue.directive('visible', function (el, binding) {
//     el.style.display = (binding.value ? 'visible' : 'none');
// });

new Vue({
    router,
    store,
    i18n,
    vuetify,
    render: h => h(App)
}).$mount("#app");
