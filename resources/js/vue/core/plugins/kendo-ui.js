import Vue from "vue";
import "@progress/kendo-theme-bootstrap/dist/all.scss";
import {Grid, GridToolbar} from '@progress/kendo-vue-grid';
import {DropDownList} from '@progress/kendo-vue-dropdowns';

Vue.component('DropDownList', DropDownList);
Vue.component('Grid', Grid);
Vue.component('GridToolbar', GridToolbar);
