import Vue from "vue";
import '@progress/kendo-ui'
// import '@progress/kendo-theme-default/dist/all.css'
import "@progress/kendo-theme-bootstrap/dist/all.scss";

import {Grid, GridInstaller} from '@progress/kendo-grid-vue-wrapper'
import {ContextMenu, LayoutInstaller} from '@progress/kendo-layout-vue-wrapper'

import JSZip from 'jszip';

Vue.use(LayoutInstaller)
Vue.use(GridInstaller);

Vue.component('Grid', Grid);
Vue.component('ContextMenu', ContextMenu);

window.JSZip = JSZip;
