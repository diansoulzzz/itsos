import Vue from 'vue'

Vue.mixin({
    methods: {
        capitalizeFirstLetter: str => str.charAt(0).toUpperCase() + str.slice(1),
        img2xUrl(val) {
            return `${val.replace(/(\.[\w\d_-]+)$/i, '@2x$1')} 2x`;
        },
        toObject(data) {
            return JSON.parse(JSON.stringify(data))
        },
        getBreadCrumb(context) {
            const routePath = context.$route.path.substr(1).split("/");
            const routeName = context.$route.path;
            let breadcrumb = [];
            routePath.map(function (value, key) {
                if (!value) {
                    value = 'Home';
                }
                let title = context.capitalizeFirstLetter(value);
                let header = value;
                if (key === routePath.length - 1) {
                    if (routePath[key - 1]) {
                        header = context.capitalizeFirstLetter(routePath[key - 1]) + " " + title;
                    }
                }
                breadcrumb.push({title: title, route: routeName, header: header});
            });
            return breadcrumb;
        }
    },
})
