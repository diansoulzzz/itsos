import ApiService from "@/core/services/api.service";

const API_PATH = 'master/mekanik/';

export const SUBMIT = API_PATH + "submitData";
export const RESET = API_PATH + "reset";
export const LOAD_DATA = API_PATH + "loadData";
export const LOAD_LIST = API_PATH + "loadList";

export const SET_STATE = API_PATH + "setState";
export const SET_DATA = API_PATH + "setData";
export const SET_LIST = API_PATH + "setList";
export const SET_ERROR = API_PATH + "setError";

const state = {
    errors: [],
    data: {},
    table: {},
};

const getters = {
    currentData(state) {
        return state.data;
    },
    currentTable(state) {
        return state.table;
    },
    currentErrors(state) {
        return state.errors;
    },
};

const actions = {
    [RESET](context) {
        context.commit(SET_STATE);
    },
    [SUBMIT](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.post(API_PATH + "detail", payload)
                .then(({data}) => {
                    context.commit(SET_DATA, data);
                    resolve(data);
                })
                .catch(({data}) => {
                    context.commit(SET_ERROR, data);
                    reject(data);
                });
        });
    },
    [LOAD_DATA](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.query(API_PATH + "detail", payload)
                .then(({data}) => {
                    context.commit(SET_DATA, data);
                    resolve(data);
                })
                .catch(({data}) => {
                    context.commit(SET_ERROR, data);
                    reject(data);
                });
        });
    },
    [LOAD_LIST](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.get(API_PATH + "list", payload)
                .then(({data}) => {
                    context.commit(SET_LIST, data);
                    resolve(data);
                })
                .catch(({data}) => {
                    context.commit(SET_ERROR, data);
                    reject(data);
                });
        });
    },
};

const mutations = {
    [SET_STATE](state) {
        state.errors = [];
        state.data = {};
        state.table = {};
    },
    [SET_ERROR](state, response) {
        state.errors = response.errors;
    },
    [SET_DATA](state, response) {
        state.data = response.data;
    },
    [SET_LIST](state, response) {
        state.table = response.table;
    },
};

export default {
    state,
    actions,
    mutations,
    getters
};
