// action types
export const SHOW_ALERT = "showAlert";
export const HIDE_ALERT = "hideAlert";

export const ADD_ALERT = "addAlert";
export const CLEAR_ALERT = "clearAlert";

export default {
    state: {
        alert: {
            type: "",
            list: [],
        }
    },
    getters: {
        alert(state) {
            return state.alert;
        },
    },
    actions: {
        [SHOW_ALERT](state, payload) {
            state.commit(ADD_ALERT, {type: payload.type, list: payload.list});
            // setTimeout(() => {
            //     state.commit(CLEAR_ALERT, payload);
            // }, 3000);
        },
        [HIDE_ALERT](state) {
            state.commit(CLEAR_ALERT);
        },
    },
    mutations: {
        [ADD_ALERT](state, alert) {
            state.alert = alert;
        },
        [CLEAR_ALERT](state) {
            state.alert = {};
        }
    }
};
