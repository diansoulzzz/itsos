import ApiService from "@/core/services/api.service";

const API_PATH = 'olap/print/';

export const SAVE_DESIGN = API_PATH + "saveDesign";
export const LOAD_DESIGN = API_PATH + "loadDesign";

export const SET_DATA = API_PATH + "setData";
export const SET_ERROR = API_PATH + "setError";

const state = {
    errors: [],
    data: {},
};

const getters = {
    currentPrintData(state) {
        return state.data;
    },
    currentPrintErrors(state) {
        return state.errors;
    },
};

const actions = {
    [SAVE_DESIGN](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.post(API_PATH, payload)
                .then(({data}) => {
                    context.commit(SET_DATA, data);
                    resolve(data);
                })
                .catch(({data}) => {
                    context.commit(SET_ERROR, data);
                    reject(data);
                });
        });
    },
    [LOAD_DESIGN](context, payload) {
        return new Promise((resolve, reject) => {
            ApiService.query(API_PATH, payload)
                .then(({data}) => {
                    context.commit(SET_DATA, data);
                    resolve(data);
                })
                .catch(({data}) => {
                    context.commit(SET_ERROR, data);
                    reject(data);
                });
        });
    },
};

const mutations = {
    [SET_ERROR](state, response) {
        state.errors = response.errors;
    },
    [SET_DATA](state, response) {
        state.data = response.data;
    },
};

export default {
    state,
    actions,
    mutations,
    getters
};
