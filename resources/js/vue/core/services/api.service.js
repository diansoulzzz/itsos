import Vue from "vue";
import axios, {AxiosRequestConfig} from "axios";
import VueAxios from "vue-axios";
import JwtService from "@/core/services/jwt.service";

const ApiService = {
    init() {
        Vue.use(VueAxios, axios);
        Vue.axios.defaults.baseURL = process.env.MIX_API_URL;
    },
    error(error) {
        if (error.response) {
            return error.response;
        }
        return error;
    },
    is_error(response) {
        return !!(response.data.errors && response.data.errors.length > 0);
    },
    /**
     * Set the default HTTP request headers
     */
    setHeader() {
        Vue.axios.defaults.headers.common[
            "Authorization"
            ] = `Bearer ${JwtService.getToken()}`;
    },

    query(resource, params) {
        return Vue.axios.get(resource, {params: params}).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    },

    /**
     * Send the GET HTTP request
     * @param resource
     * @param slug
     * @returns {*}
     */
    get(resource, slug = "") {
        return Vue.axios.get(`${resource}/${slug}`).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    },

    /**
     * Set the POST HTTP request
     * @param resource
     * @param params
     * @returns {*}
     */
    post(resource, params) {
        return Vue.axios.post(`${resource}`, params).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    },

    /**
     * Send the UPDATE HTTP request
     * @param resource
     * @param slug
     * @param params
     * @returns {IDBRequest<IDBValidKey> | Promise<void>}
     */
    update(resource, slug, params) {
        return Vue.axios.put(`${resource}/${slug}`, params).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    },

    /**
     * Send the PUT HTTP request
     * @param resource
     * @param params
     * @returns {IDBRequest<IDBValidKey> | Promise<void>}
     */
    put(resource, params) {
        return Vue.axios.put(`${resource}`, params).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    },

    /**
     * Send the DELETE HTTP request
     * @param resource
     * @returns {*}
     */
    delete(resource) {
        return Vue.axios.delete(resource).then(response => {
            if (this.is_error(response)) {
                throw this.error(response);
            }
            return response;
        }).catch(error => {
            throw this.error(error);
        });
    }
};

export default ApiService;
