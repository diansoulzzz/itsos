import Vue from "vue";
import Router from "vue-router";

import Error404 from '@/view/pages/error/Error-1'
import LayoutLogin from '@/view/pages/auth/Auth';
import LayoutDefault from '@/view/layout/Layout';
import Home from '@/view/pages/admin/home/IndexDetail';
import MasterMekanikDetail from '@/view/pages/admin/master/mekanik/IndexDetail';
import MasterMekanikList from '@/view/pages/admin/master/mekanik/IndexList';
import Login from '@/view/pages/auth/Login';
import Register from '@/view/pages/auth/Register';

import PrintDesigner from '@/view/pages/olap/print/PrintDesigner';
import {LOGOUT} from "@/core/services/store/auth.module";

// const Route404 = {name: "404", path: "/404", component: Error404};
export const Route404 = "404";
export const RouteHome = 'home';
export const RouteLogin = 'login';
export const RouteLogout = 'logout';
export const RouteRegister = 'register';
export const RouteMekanikDetail = 'master.mekanik.detail';
export const RouteMekanikList = 'master.mekanik.list';
export const RoutePrintDesigner = 'print.designer';

Vue.use(Router);
export default new Router({
    routes: [
        {
            path: "*",
            redirect: {name: Route404},
        },
        {
            name: RouteLogout,
            path: "/logout",
            meta: {dispatch: LOGOUT}
        },
        {
            name: Route404,
            path: "/404",
            component: Error404,
            meta: {title: 'Error 404'}
        },
        {
            name: RoutePrintDesigner,
            path: "/print/designer",
            component: PrintDesigner,
            props: (route) => ({
                print: route.query.print,
                id: route.query.id,
                hid: route.query.hid,
                isDesigner: (!!route.query.hm)
            }),
            meta: {title: 'Print'}
        },
        {
            path: '/',
            redirect: {name: RouteHome},
            component: LayoutDefault,
            children: [
                {
                    name: RouteHome,
                    path: '/',
                    component: Home,
                    meta: {title: 'Home'},
                },
                {
                    name: RouteMekanikDetail,
                    path: '/master/mekanik/detail',
                    component: MasterMekanikDetail,
                    meta: {title: 'Mekanik Detail'},
                    props: (route) => ({
                        id: route.query.id,
                    }),
                },
                {
                    name: RouteMekanikList,
                    path: '/master/mekanik/list',
                    component: MasterMekanikList,
                    props: true,
                    meta: {title: 'Mekanik List'},
                },
            ]
        },
        {
            path: "/",
            component: LayoutLogin,
            children: [
                {
                    name: RouteLogin,
                    path: "/login",
                    component: Login,
                    meta: {title: 'Login'},
                },
                {
                    name: RouteRegister,
                    path: "/register",
                    component: Register,
                    meta: {title: 'Register'},
                }
            ]
        },
    ]
});
