const path = require('path')
// const webpack = require('webpack');

module.exports = {
    // plugins: [
    //     new webpack.IgnorePlugin(/\.\/locale$/)
    // ],
    // node: {
    //     'fs': "empty",
    //     'child_process': "empty",
    //     'dgram': "empty",
    //     'dns': "empty",
    //     'net': "empty",
    //     'pg': "empty",
    //     'tls': "empty",
    //     'pg-native': "empty",
    // },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.runtime.esm',
            '@': path.resolve(__dirname, 'resources/js/vue'),
            "@stimulsoft": path.resolve(__dirname, 'public/vendor/core/stimulsoft'),
            //dummy
            // 'fs': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'child_process': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'dgram': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'dns': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'net': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'pg': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'tls': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // 'pg-native': path.resolve(__dirname, 'resources/js/vue/assets/js/components/empty.js'),
            // //stimulsoft
            // 'report': path.resolve(__dirname, 'resources/js/vue/assets/plugins/stimulsoft/scripts/stimulsoft.reports'),
            // 'report-maps': path.resolve(__dirname, 'resources/js/vue/assets/plugins/stimulsoft/scripts/stimulsoft.reports.maps'),
            // // 'dashboard': path.resolve(__dirname, 'resources/js/vue/assets/plugins/stimulsoft/scripts/stimulsoft.dashboards'),
            // 'viewer': path.resolve(__dirname, 'resources/js/vue/assets/plugins/stimulsoft/scripts/stimulsoft.viewer'),
            // 'designer': path.resolve(__dirname, 'resources/js/vue/assets/plugins/stimulsoft/scripts/stimulsoft.designer'),
        },
    },
}
